![picutre](lm.jpg)

# Simultaneous multi-descent regression and feature learning forfacial landmarking in depth images

Support code for the paper [Simultaneous multi-descent regression and feature learning forfacial landmarking in depth images](http://link.springer.com/article/10.1007/s00521-019-04529-7) (accepted for publication in Neural Computing and Applications in 2019).

## Quick start

This demo enables training and testing a face landmarking model on depth facial images. Two methods from the paper are implemented, namely GRID (Gated multiple RIdge Descent) and SMUF(Simultaneous learning of MUltiple descent directions and binary Features). Both methods can be trained/tested on face images from the Bosphorus dataset by running the [main.m](./main.m) script.

## Reference
```
@Article{Krizaj2019,
author="Kri{\v{z}}aj, Janez
and Peer, Peter
and {\v{S}}truc, Vitomir
and Dobri{\v{s}}ek, Simon",
title="Simultaneous multi-descent regression and feature learning for facial landmarking in depth images",
journal="Neural Computing and Applications",
year="2019",
month="Oct",
day="23",
issn="1433-3058",
doi="10.1007/s00521-019-04529-7",
url="https://doi.org/10.1007/s00521-019-04529-7"
}
```
