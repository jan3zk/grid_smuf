classdef Limage < handle
% LIMAGE Class for storing an image with annotated landmarks
properties
    m_lmarks
    data
    data_aux
    orig_scale
    bbox
end
properties(GetAccess = public, SetAccess = protected)
    gt_lmarks
    image_fpath
    lmark_fpath
    lmark_labels
end
properties (Access = protected)
    crop_offset
end

methods
    
    function self = Limage(varargin)
        % SELF = LIMAGE()
        % SELF = LIMAGE(DATA)
        % SELF = LIMAGE(DATA, LMRK)
        %
        % Inputs:
        % DATA is one of the following:
        %      - 'path\to\images\*.jpg',
        %      -  [{cell/array/of/strings/of/image/paths.jpg}].
        %      - image matrix <W x H x 3> or <W x H>
        %      *(allowed formats: bmp, jpg, tiff, png, ppm, gif, ico,
        %        avi, ...)
        %
        % LMRK is one of the following:
        %      - 'path\to\lmarks\*.pts',
        %      - [{cell/array/of/strings/of/lmrks/paths.pts}].
        %      - landmark matrix <N x 2>
        %      *(allowed formats: pts, lm3, lm2)
        %
        % Outputs:
        % SELF: Limage object or Limage object array
                
        p = inputParser;
        addOptional(p, 'data', [], @(x)isnumeric(x)||iscell(x)||ischar(x))
        addOptional(p, 'gt_lmarks', [], @(x)isnumeric(x)||iscell(x)||ischar(x))
        addOptional(p, 'm_lmarks', {[]}, @(x)isnumeric(x)||iscell(x))
        addOptional(p, 'bbox', [], @isnumeric)
        addOptional(p, 'lmark_labels', [], @iscell)
        addOptional(p, 'image_fpath', [], @ischar)
        addOptional(p, 'orig_scale', 1, @isnumeric)
        parse(p, varargin{:})
        data = p.Results.data;
        gt_lmarks = p.Results.gt_lmarks;
        m_lmarks = p.Results.m_lmarks;
        bbox = p.Results.bbox;
        labels = p.Results.lmark_labels;
        fpath = p.Results.image_fpath;
        oscale = p.Results.orig_scale;

        if ischar(data) && ischar(gt_lmarks) 
            img_names = cellstr(ls(data));
            lmrk_names = cellstr(ls(gt_lmarks));
            img_path = fileparts(data);
            lmrk_path = fileparts(gt_lmarks);
            self(length(lmrk_names)) = Limage;
            for c = 1 : length(lmrk_names)
                self(c).image_fpath = [img_path '\' img_names{c}];
                self(c).lmark_fpath = [lmrk_path '\' lmrk_names{c}];
                gt_lmrk = Limage.ptsread(self(c).lmark_fpath);
                self(c).gt_lmarks = gt_lmrk;
                self(c).m_lmarks = m_lmarks;
                self(c).crop_offset = zeros(size(gt_lmrk));
            end

        elseif iscell(data) && iscell(gt_lmarks) 
            self(length(gt_lmarks)) = Limage;
            for c = 1 : length(gt_lmarks)
                self(c).image_fpath = data{c};
                self(c).lmark_fpath = gt_lmarks{c};
                [gt_lmrk, labels] = Limage.ptsread(self(c).lmark_fpath);
                self(c).gt_lmarks = gt_lmrk;
                self(c).lmark_labels = labels;
                self(c).m_lmarks = m_lmarks;
                self(c).crop_offset = zeros(size(gt_lmrk));
            end

        elseif isnumeric(data)&&~isempty(data)
            self.data = data;
            if ~isempty(m_lmarks)
                self.m_lmarks = m_lmarks;
            end
            if ~isempty(bbox)
                self.bbox = bbox;
            end
            if ~isempty(gt_lmarks) && isnumeric(gt_lmarks)
                self.gt_lmarks = gt_lmarks;
                self.crop_offset = zeros(size(gt_lmarks));
            end
            self.lmark_labels = labels;
            self.image_fpath = fpath;
        end
        [self.orig_scale] = deal(oscale);
        
    end
    
    function lmrk = get.gt_lmarks(self)
        if ~isempty(self.image_fpath) && any(strcmp(self.image_fpath(end-3:end),{'.bnt','.abs'}))
            self.load_into_mem();
        end
        lmrk = self.gt_lmarks;
    end

    function data = get.data(self)
       if isempty(self.data) && ~isempty(self.image_fpath)
            imformat = imformats;
            extensions = [imformat.ext];
            [~,~,ext] = fileparts(self.image_fpath);
            if any(strcmp(ext(2:end),extensions))
                data = imread(self.image_fpath);
            elseif strcmp(ext,'.avi')
                [~, fnumber, ~] = fileparts(self.lmark_fpath);
                [~, fsampling, ~] = fileparts(self.image_fpath);
                fnumber = str2double(fnumber);
                fsampling = str2double(regexp(fsampling, '\d+', 'match'));
                data = Limage.video_buffer(self.image_fpath,...
                                           fnumber, fsampling);
            elseif strcmp(ext,'.bnt')
                data = bntread(self);
            elseif strcmp(ext,'.abs')
                data = absread(self);
            else
                error('Unsupported extension of the file containing image data.')
            end
        elseif ~isempty(self.data)
           data = self.data;
        else
           error(['Image data not defined yet. You should provide ',...
                  'path to the stored image or pass an image array.'])
        end
    end
    
    function load_into_mem(self)
        for c = 1 : length(self)
            self(c).data = self(c).data;
        end
    end
    
    function crop_to_lmarks(self, varargin)
        % Only the area centered at landmarks is retained. If it wasn't 
        % already, image is then loaded into memory.
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'prenorm', [], @(x)ischar(x)||isnumeric(x))
        addOptional(p, 'crop_ratio', 0.4, @isnumeric)
        parse(p, self, varargin{:})
        self = p.Results.self;
        pnrm = p.Results.prenorm;
        crop_ratio = p.Results.crop_ratio;
        
        for c = 1 : length(self)
            tmp_data = self(c).data;
            min_lmrk = min(self(c).gt_lmarks);
            max_lmrk = max(self(c).gt_lmarks);
            lmrk_range = max(max_lmrk-min_lmrk);
            min_v = round(min_lmrk(2)-lmrk_range*crop_ratio);
            min_h = round(min_lmrk(1)-lmrk_range*crop_ratio);
            max_v = round(max_lmrk(2)+lmrk_range*crop_ratio);
            max_h = round(max_lmrk(1)+lmrk_range*crop_ratio);
            if min_v < 1
                min_v = 1;
            end
            if min_h < 1
                min_h = 1;
            end
            if max_v > size(tmp_data, 1)
                max_v = size(tmp_data, 1);
            end
            if max_h > size(tmp_data, 2)
               max_h = size(tmp_data, 2);
            end
            self(c).data = tmp_data(min_v: max_v,...
                                        min_h: max_h, :);
            lmrk_offset = repmat([min_h min_v],...
                                  size(self(c).gt_lmarks, 1), 1);
            self(c).gt_lmarks = self(c).gt_lmarks - lmrk_offset;
            if any(~cellfun(@isempty, self(c).m_lmarks))
                for p = 1 : length(self(c).m_lmarks)
                    self(c).m_lmarks{p} = self(c).m_lmarks{p} - lmrk_offset;
                end
            end
            if ~isempty(self(c).bbox)
                self(c).bbox(1:2) = self(c).bbox(1:2) - [min_h min_v]';
            end
            self(c).crop_offset = lmrk_offset;
            if ~isempty(pnrm)
                self(c).uint8gray();
            end
            if length (self) > 1
                if ~isempty(pnrm)
                    fprintf('Crop and normalize (%d/%d)\n', c, length(self))    
                else
                    fprintf('Crop (%d/%d)\n', c, length(self))
                end
            end
        end
    end
    
    function varargout = face_detect(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'detector_type', 'npd', @(x)ischar(x)||@(x)isstruct(x))
        addOptional(p, 'train', 0, @isnumeric)
        parse(p, self, varargin{:})
        self = p.Results.self;
        detector_type = p.Results.detector_type;
        train = p.Results.train;
        
        if strcmp(detector_type, 'npd')
            load('model_frontal.mat','npdModel');
            fd_model.frontal = npdModel;
            load('model_unconstrain.mat', 'npdModel');
            fd_model.uncon = npdModel;

            opts.minFace = 60;
            for c = 1 : length(self)
                rect = fd.DetectFace(fd_model.frontal, self(c).data, opts);
                if isempty(rect)
                    rect = fd.DetectFace(fd_model.uncon, self(c).data, opts);
                end
                if length(rect)>1
                    rect = rect([rect.size] == max([rect.size]));
                end
                if ~isempty(rect)
                    rect = [rect(1).col rect(1).row rect(1).size rect(1).size]';
                end
                self(c).bbox = rect;
                if length(self) > 1
                    fprintf('Face detection %d/%d\n', c, length(self))
                end
            end
            
        elseif strcmp(detector_type, 'vj')
            for c = 1 : length(self)
                img = self(c).data;
                rect = step(vision.CascadeObjectDetector(...
                    'FrontalFaceCART', 'MinSize', [70 70]), img);
                if isempty(rect)
                    rect = step(vision.CascadeObjectDetector(...
                        'FrontalFaceLBP', 'MinSize', [70 70]), img);
                end
                if isempty(rect)
                    rect = step(vision.CascadeObjectDetector(...
                        'ProfileFace', 'MinSize', [70 70]), img);
                end
                if size(rect,1) > 1
                    rect = rect(rect(:,3) == max(rect(:, 3)), :);
                end
                self(c).bbox = rect;
                if length(self) > 1
                    fprintf('Face detection %d/%d\n', c, length(self))
                end
            end
            
        elseif strcmp(detector_type, 'cod') % cascade object detector
            border_size = 100;
            if train
                neg_folder = 'data\cascaded_detector\nonFaces\';
                if 1
                    pos_folder = 'data\cascaded_detector\Faces\';
                    for c = 1 : length(self)
                        [~,name,~] = fileparts(self(c).image_fpath);
                        full_path = [pos_folder,name,'.jpg'];
                        img = self(c).data;
                        box = self(c).face_detect('gt');
                        img = padarray(img,[border_size border_size], mode(img(:)), 'both');
                        box(1:2) = box(1:2) + border_size;
                        if box(1) < 1, box(1) = 1; end
                        if box(2) < 1, box(2) = 1; end
                        if box(1)+box(3) > size(img,2), box(3) = size(img,2)-box(1); end
                        if box(2)+box(4) > size(img,1), box(4) = size(img,1)-box(4); end
                        imwrite(img, full_path)
                        tdata(c).imageFilename = full_path;
                        tdata(c).objectBoundingBoxes = box';
                        row1 = round(box(2)-box(4));
                        row2 = round(box(2)+box(4));
                        col1 = round(box(1)-box(3));
                        col2 = round(box(1)+box(3)+box(3));
                        if col1 < 1, col1 = 1; end
                        if col2 > size(img, 2), col2 = size(img, 2); end
                        if row1 < 1, row1 = 1; end
                        if row2 > size(img, 1), row2 = size(img, 1); end
                        img(row1 : row2, col1 : col2) = mode(img(:));
                        imwrite(img, [neg_folder,name,'_extBox2.jpg'])
                    end
                    save('data\cascaded_detector\face3D','tdata')
                else
                    load('data\cascaded_detector\face3D')
                end
                trainCascadeObjectDetector(...
                    'face3DDetector.xml', tdata, neg_folder,...
                    'FalseAlarmRate',0.25,'NumCascadeStages',20);   
                movefile face3DDetector.xml data\cascaded_detector\face3DDetector.xml
            else
                detector = vision.CascadeObjectDetector('data\cascaded_detector\face3DDetector.xml');
                for c = 1 : length(self)
                    img = self(c).data;
                    img = padarray(img,[border_size border_size], mode(img(:)), 'both');
                    box = step(detector,img);
                    if ~isempty(box)
                        box = mean(box(box(:,3) > 50,:),1);
                        box(1:2) = box(1:2) - border_size;
                        self(c).bbox = box;
                    else
                        self(c).bbox = box;
                        fprintf('Couldn''t detect any face on image %d.\n',c)
                    end
                    fprintf('Face detection %d/%d\n', c, length(self))
                end
            end
            
        elseif strcmp(detector_type, 'gt')
            labels_ = {'Outer left eye corner',...
                'Inner left eye corner',...
                'Inner right eye corner',...
                'Outer right eye corner',...
                'Nose tip',...
                'Left mouth corner',...
                'Right mouth corner',...
                'Chin middle'};
            for c = 1 : length(self)
                [~,idx] = ismember(self(c).lmark_labels,labels_);
                gt_lmrk = self(c).gt_lmarks(idx~=0,:);
                rsize = max(max(gt_lmrk) - min(gt_lmrk));
                rcenter = mean(gt_lmrk);
                box = [rcenter(1) - rsize/2; ...
                                rcenter(2) - rsize/2; ...
                                rsize; rsize];
                if nargout > 0
                    varargout{1} = box;
                else
                    self(c).bbox = box;
                end
                if length(self) > 1
                    fprintf('Face detection %d/%d\n', c, length(self))
                end
            end
            
        elseif strcmp(detector_type, 'clst') %clustering based, 3D only
            for c = 1 : length(self)
                img = self(c).data;
                img1 = [diff(img); NaN*zeros(1,size(img,2))];
                img1(abs(img1)>2) = NaN; 
                img1 = filter2(fspecial('average',2), img1); 
                img1(~isnan(img1)) = 1;
                img1(isnan(img1)) = 0;
                img(~img1) = NaN;
                clst = io.cluster_detect(img);
                [y,x] = find(clst);
                rcenter = [mean(x) mean(y)]; 
                rsize = [sum(clst,1) sum(clst,2)'];
                rsize(rsize==0) = [];
                rsize = mean(rsize);
                self(c).bbox = [rcenter(1) - rsize/2; ...
                                rcenter(2) - rsize/2; ...
                                rsize; rsize];
                if length(self) > 1
                    fprintf('Face detection %d/%d\n', c, length(self))
                end
            end
        elseif strcmp(detector_type, 'pr') %profile regressor, 3D only
            pcanorm = false;
            len = 15;
            if train
                box = zeros(4,length(self));
                prof = zeros(3*len,length(self));
                for c = 1 : length(self)
                    bbox_tmp = self(c).bbox;
                    self(c).face_detect('gt');
                    box(:,c) = self(c).bbox;
                    self(c).bbox = bbox_tmp;
                    depth = self(c).data;
                    silh = imfill(depth~=mode(depth(:)),'holes'); %silh = imfill(~isnan(depth),'holes'); %silh = im2bw(depth-min(depth(:)),0);
                    prof_left = silh & cumsum(silh,2) == 1;
                    [~,prof_left] = find(prof_left);
                    prof_left = prof_left(round(linspace(1,length(prof_left),len)));
                    prof_right = fliplr(fliplr(silh) & cumsum(fliplr(silh),2) == 1);
                    [~,prof_right] = find(prof_right);
                    prof_right = prof_right(round(linspace(1,length(prof_right),len)));
                    prof_up = silh & cumsum(silh,1) == 1;
                    [prof_up,~] = find(prof_up);
                    prof_up = prof_up(round(linspace(1,length(prof_up),len)));
                    prof(:,c) = [prof_left; prof_right; prof_up];
                end
                prof_mean = mean(prof, 2);
                prof = prof - repmat(prof_mean, 1, size(prof, 2));
                box_mean = mean(box, 2);
                box = box - repmat(box_mean, 1, size(box,2));
                if pcanorm
                    [coeff, ~, ~, ~, explained] = pca(prof');
                    len = cumsum(explained) < 98;
                    pcab = coeff(:, len);
                    prof = pcab'*prof;
                else
                    pcab = [];
                end
                reg =  box*prof'/(prof*prof' + .1*mean(diag(prof*prof')).*eye(size(prof, 1)));
                fd_out = struct('reg',reg, 'prof_mean',prof_mean, 'box_mean',box_mean, 'pcab',pcab);
                varargout{1} = fd_out;
                save('models\face_detector\profile_regesor.mat','prof_mean', 'box_mean', 'reg','pcab')
            else
                trn = load('models\face_detector\profile_regesor.mat');
                for c = 1 : length(self)
                    depth = self(c).data;
                    silh = imfill(depth~=mode(depth(:)),'holes'); %silh = imfill(~isnan(depth),'holes'); %silh = im2bw(depth-min(depth(:)),0);
                    prof_left = silh & cumsum(silh,2) == 1;
                    [~,prof_left] = find(prof_left);
                    prof_left = prof_left(round(linspace(1,length(prof_left),len)));
                    prof_right = fliplr(fliplr(silh) & cumsum(fliplr(silh),2) == 1);
                    [~,prof_right] = find(prof_right);
                    prof_right = prof_right(round(linspace(1,length(prof_right),len)));
                    prof_up = silh & cumsum(silh,1) == 1;
                    [prof_up,~] = find(prof_up);
                    prof_up = prof_up(round(linspace(1,length(prof_up),len)));
                    prof = [prof_left; prof_right; prof_up];
                    prof = prof - trn.prof_mean;
                    if pcanorm
                        prof = trn.pcab'*prof;
                    end
                    self(c).bbox = trn.reg*prof + trn.box_mean;
                end
            end
        elseif strcmp(detector_type, 'ib') %image border as detection box
            for c = 1 : length(self)
                rcenter = [size(self(c).data,2)/2; size(self(c).data,1)/2];
                %rcenter = regionprops(~isnan(self(c).data), 'centroid');
                %rcenter = rcenter.Centroid;
                rsize = min(size(self(c).data));
                self(c).bbox = [rcenter(1) - rsize/2; ...
                                rcenter(2) - rsize/2; ...
                                rsize; rsize];
            end
        else
            error('Unknown detector type.')
        end
    end
    
    function discard_lmarks(self, lmidx)
       for c = 1 : length(self)
           if ~isempty(self(c).gt_lmarks)
               self(c).gt_lmarks = self(c).gt_lmarks(lmidx, :);
           end
           for d = 1 : length(self(c).m_lmarks)
               self(c).m_lmarks{d} = self(c).m_lmarks{d}(lmidx, :);
           end
       end
    end
    
    function self_refined = remove_misdetections(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'scl_factor', 1.0, @isnumeric)
        addOptional(p, 'verbose', 0)
        parse(p, self, varargin{:})
        self = p.Results.self;
        scl_factor = p.Results.scl_factor;
        verbose = p.Results.verbose;
        
        detect = false(length(self), 1);
        mean_lm_center = mean(cell2mat(cellfun(@mean,{self.gt_lmarks},'un',0)'));
        mean_box_center = mean(cell2mat({self.bbox}),2)';
        mean_box_center = mean_box_center(1:2) + mean_box_center(3:4)/2;
        mean_off = mean_lm_center - mean_box_center;
        for c = 1 : length(self)
            if ~isempty(self(c).bbox)
                bbox_center = [self(c).bbox(1) + self(c).bbox(3)/2,...
                               self(c).bbox(2) + self(c).bbox(4)/2];
                lmrk_center = mean(self(c).gt_lmarks);
                lmrk_range = max( max(self(c).gt_lmarks) - ...
                                      min(self(c).gt_lmarks) );
                bbox_range = mean( self(c).bbox(3:4) );
                off = sqrt( sum( (lmrk_center - bbox_center - mean_off).^2 )) ;
                scl = bbox_range/lmrk_range/scl_factor;
                if off < lmrk_range/2 && abs(1-scl) < .4
                    detect(c) = true;
                else
                    if verbose
                        self(c).view
                        pause
                    end
                end
            end
        end
        fprintf('%d misdetections found.\n', sum(~detect))
        self_refined = self(detect);
    end
    
    function varargout = lmarks_from_bb(self, varargin)
        % LMARKS_FROM_BB initializes landmark locations (m_lmarks property)
        % based on the location of the bounding box returned by the face 
        % detector. If an object array is passed as an imput argument it 
        % returns learned relations between bounding boxes and landmarks.
        %
        % Usage:
        %   lmarks_from_bb(self)
        %   lmarks_from_bb(self, bb2lm)
        %   bb2lm = lmarks_from_bb(self)
        %   alt_lmarks = lmarks_from_bb(self, bb2lm, alt_box)
        %           
        
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'bb2lm', struct('avg_lmrk', {},...
                                       'mean_bb_size', {},...
                                       'mean_offset', {}), @isstruct)
        addOptional(p, 'alt_bbox', [])
        addOptional(p, 'scale_factor', 1.0, @isnumeric)
        parse(p, self, varargin{:})
        self = p.Results.self;
        bb2lm = p.Results.bb2lm;
        alt_bbox = p.Results.alt_bbox;
        scale_factor = p.Results.scale_factor;
        
        if isempty(bb2lm)
            offset = NaN.*ones(length(self),2);
            bb_size = NaN.*ones(length(self),1);
            for c = 1 : length(self)
                if ~isempty(self(c).bbox)
                    bbox_center = [self(c).bbox(1) + self(c).bbox(3)/2,...
                                   self(c).bbox(2) + self(c).bbox(4)/2];
                    lmrk_center = mean(self(c).gt_lmarks);
                    bb_size(c) = mean( self(c).bbox(3:4) );
                    offset(c,:) = (lmrk_center - bbox_center)/bb_size(c);
                end
            end
            bb2lm(1).mean_bb_size = nanmean(bb_size);
            bb2lm(1).mean_offset = nanmean(offset);
            bb2lm(1).avg_lmrk = self.landmark_average();
            varargout{1} = bb2lm;
        end
        for c = 1 : length(self)
            if ~isempty(self(c).bbox)
                if isempty(alt_bbox)
                    bbox_tmp = self(c).bbox;
                else
                    if length(self) > 1
                        bbox_tmp = alt_bbox{c};
                    else
                        bbox_tmp = alt_bbox;
                    end
                end
                bb_size_tmp = mean(bbox_tmp(3:4));
                bbox_tmp = bbox_tmp(:)';
                offset_tmp = bbox_tmp(1:2) + bbox_tmp(3:4)/2 + ...
                    bb_size_tmp.*bb2lm.mean_offset;
                m_lmarks_tmp = (bb2lm.avg_lmrk - ...
                    repmat(mean(bb2lm.avg_lmrk), size(bb2lm.avg_lmrk, 1), 1)).*...
                    (bb_size_tmp/bb2lm.mean_bb_size*scale_factor) +...
                    repmat(offset_tmp, size(bb2lm.avg_lmrk, 1), 1);
                if isempty(alt_bbox)
                    self(c).m_lmarks = [];
                    self(c).m_lmarks{1} = m_lmarks_tmp;
                else
                    varargout{1} = m_lmarks_tmp;
                end
            else
                warning('Object %d doesn''t have a bounding box.', c)
            end
        end
    end
    
    function random_sampling(self, bb2lm, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addRequired(p, 'bb2lm')
        addOptional(p, 'permuts', 10)
        addOptional(p, 'disp_var', 0.08)
        addOptional(p, 'scale_var', 0.02)
        parse(p, self, bb2lm, varargin{:})
        self = p.Results.self;
        bb2lm = p.Results.bb2lm;
        permuts = p.Results.permuts;
        disp_var = p.Results.disp_var;
        scale_var = p.Results.scale_var;
        
        for c = 1 : length(self)
            box = self(c).bbox;
            for p = 2 : permuts
                permut_box = normrnd(box(1:3),...
                    [box(3)*disp_var; box(3)*disp_var; box(3)*scale_var]);
                permut_box(1:2) = permut_box(1:2)+(box(3)-permut_box(3))/2;
                self(c).m_lmarks{p} = self(c).lmarks_from_bb(...
                    bb2lm, 'alt_bbox',[permut_box; permut_box(3)]);
            end
        end
    end

    function random_sampling_gt(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'permuts', 5, @isnumeric)
        addOptional(p, 'disp_var', 0.3)
        addOptional(p, 'scale_var', 0.01)
        parse(p, self, varargin{:})
        permuts = p.Results.permuts;
        disp_var = p.Results.disp_var;
        scale_var = p.Results.scale_var;

        for c = 1 : length(self)
            offp = mean(self(c).gt_lmarks-self(c).m_lmarks{1});
            scalep =  (max(self(c).m_lmarks{1})-min(self(c).m_lmarks{1}))./...
                      (max(self(c).gt_lmarks) - min(self(c).gt_lmarks));
            if length(disp_var) == 1
                varp = [disp_var disp_var].*...
                    sqrt(sum((self(c).gt_lmarks-self(c).m_lmarks{1}).^2));
            else
                varp = [disp_var(1) disp_var(2)].*...
                    sqrt(sum((self(c).gt_lmarks-self(c).m_lmarks{1}).^2));
            end
            samples = zeros(permuts,4);
            for k = 1 : permuts
                samples(k,:) = normrnd([scalep(1),scalep(2),offp(1),offp(2)],[scale_var*scalep(1),scale_var*scalep(2),varp(1),varp(2)]);
            end
            mn0 = mean(self(c).m_lmarks{1});
            lmrks{1} = self(c).m_lmarks{1};
            for k = 2 : permuts
                lmrk = [];
                for j = 1 : size(self(c).m_lmarks{1},1)
                    lmrk(j,:) = [(self(c).m_lmarks{1}(j,1)-mn0(1))/samples(k-1,1)+mn0(1)+samples(k-1,3),...
                                    (self(c).m_lmarks{1}(j,2)-mn0(2))/samples(k-1,2)+mn0(2)+samples(k-1,4)];
                end
                lmrks{k} = lmrk;
            end
            self(c).m_lmarks = lmrks;
        end
    end
    
    function random_sampling_from_train(self, bb2lm, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addRequired(p, 'bb2lm')
        addOptional(p, 'nsamp', 10)
        parse(p, self, bb2lm, varargin{:})
        self = p.Results.self;
        nsamp = p.Results.nsamp;
        for c = 1 : length(self)
            mshape = self(c).m_lmarks{1};
            msize = max(mshape(:) - min(mshape(:)));
            idx = randperm(length(self));
            idx(idx==c) = [];
            for p = 2 : nsamp
                gshape = self(idx(p)).gt_lmarks;
                gsize = max(gshape(:)) - min(gshape(:));
                self(c).m_lmarks{p} = (gshape - repmat(mean(gshape),size(gshape,1),1))./...
                    gsize*msize + repmat(mean(mshape),size(mshape,1),1);
            end
        end
    end
    
    function random_sampling_shape(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'permuts', 10)
        addOptional(p, 'disp_var', 0.05)
        addOptional(p, 'scale_var', 0.5)
        parse(p, self, varargin{:})
        self = p.Results.self;
        permuts = p.Results.permuts;
        disp_var = p.Results.disp_var;
        scale_var = p.Results.scale_var;
        
        for c = 1 : length(self)
            lshape = self(c).m_lmarks{1};
            lsize = max(lshape(:)-min(lshape(:)));
            for p = 2 : permuts
                perm = normrnd([0 0 1],...
                               [disp_var*lsize disp_var*lsize scale_var]);
                off = repmat(mean(lshape),size(lshape,1),1);
                lshapet = (lshape - off)*perm(3) + off;
                lshapet(:,1) = lshapet(:,1) + perm(1);
                lshapet(:,2) = lshapet(:,2) + perm(2);
                self(c).m_lmarks{p} = lshapet;
            end
        end
    end
    
    function self_aug = generate_additional_samples(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addParameter(p, 'rotate', [])
        addParameter(p, 'mirror', 'Bosphorus')
        parse(p, self, varargin{:})
        self = p.Results.self;
        rotate = p.Results.rotate;
        mirror = p.Results.mirror;
        
        t = 1;
        if mirror
            for c = 1 : length(self)
                if strcmp(mirror, 'Bosphorus')
                    cstr = cellfun(@(x)strfind(self(c).image_fpath,x),...
                           {'L10','R10','L20','R20','L30','R30','L45','R45','L90','R90'},'un',0);
                    if any(~cellfun(@isempty,cstr))
                        flip_data = fliplr(self(c).data);
                        flip_gt_lmarks =  [size(flip_data,2)-self(c).gt_lmarks(:,1) self(c).gt_lmarks(:,2)];
                        cidx = cstr{~cellfun(@isempty,cstr)};
                        if strcmp(self(c).image_fpath(cidx), 'L')
                            
                            fpath = [self(c).image_fpath(1:cidx-1), 'R',...
                                                   self(c).image_fpath(cidx+1:end-4),...
                                                   '_mirrored' self(c).image_fpath(end-3:end)];
                        elseif strcmp(self(c).image_fpath(cidx), 'R')
                            fpath = [self(c).image_fpath(1:cidx-1), 'L',...
                                                   self(c).image_fpath(cidx+1:end-4),...
                                                   '_mirrored' self(c).image_fpath(end-3:end)];
                        else
                            error('Something went wrong with image mirroring');
                        end
                        labels = self(c).lmark_labels;
                        for d = 1 : length(self(c).lmark_labels)
                              if ~isempty(strfind(self(c).lmark_labels{d}, 'left'))
                                  labels{d} = strrep(self(c).lmark_labels{d}, 'left', 'right');
                              elseif ~isempty(strfind(self(c).lmark_labels{d}, 'Left'))
                                  labels{d} = strrep(self(c).lmark_labels{d}, 'Left', 'Right');
                              elseif ~isempty(strfind(self(c).lmark_labels{d}, 'right'))
                                  labels{d} = strrep(self(c).lmark_labels{d}, 'right', 'left');
                              elseif ~isempty(strfind(self(c).lmark_labels{d}, 'Right'))
                                  labels{d} = strrep(self(c).lmark_labels{d}, 'Right', 'Left');
                              end
                        end
                        flip_m_lmarks = {[]};
                        for p = 1 : length(self(c).m_lmarks)
                            if ~isempty(self(c).m_lmarks{p})
                                flip_m_lmarks{p} = [size(flip_data,2)-self(c).m_lmarks{p}(:,1) self(c).m_lmarks{p}(:,2)];
                            end
                        end
                        flip_bbox = [];
                        if ~isempty(self(c).bbox)
                            flip_bbox = self(c).bbox;
                            flip_bbox(1) = size(self(c).data, 2) - flip_bbox(1)...
                                           - flip_bbox(3) + 1;
                        end
                        self_aug(t) = Limage(flip_data,...
                                             flip_gt_lmarks,...
                                             flip_m_lmarks,...
                                             flip_bbox,...
                                             labels,...
                                             fpath);
                        t = t + 1;
                        fprintf('Data augmentation - mirroring (%d/%d)\n', c, length(self))
                    end
                elseif strcmp(mirror, 'und')
                    flip_data = fliplr(self(c).data);
                    flip_gt_lmarks =  [size(flip_data,2)-self(c).gt_lmarks(:,1) self(c).gt_lmarks(:,2)];
                    fpath = [self(c).image_fpath(1:end-4),...
                             '_mirrored' self(c).image_fpath(end-3:end)];
                    labels = self(c).lmark_labels;
                    for d = 1 : length(self(c).lmark_labels)
                          if ~isempty(strfind(self(c).lmark_labels{d}, 'left'))
                              labels{d} = strrep(self(c).lmark_labels{d}, 'left', 'right');
                          elseif ~isempty(strfind(self(c).lmark_labels{d}, 'Left'))
                              labels{d} = strrep(self(c).lmark_labels{d}, 'Left', 'Right');
                          elseif ~isempty(strfind(self(c).lmark_labels{d}, 'right'))
                              labels{d} = strrep(self(c).lmark_labels{d}, 'right', 'left');
                          elseif ~isempty(strfind(self(c).lmark_labels{d}, 'Right'))
                              labels{d} = strrep(self(c).lmark_labels{d}, 'Right', 'Left');
                          end
                    end
                    flip_m_lmarks = {[]};
                    for p = 1 : length(self(c).m_lmarks)
                        if ~isempty(self(c).m_lmarks{p})
                            flip_m_lmarks{p} = [size(flip_data,2)-self(c).m_lmarks{p}(:,1) self(c).m_lmarks{p}(:,2)];
                        end
                    end
                    flip_bbox = [];
                    if ~isempty(self(c).bbox)
                        flip_bbox = self(c).bbox;
                        flip_bbox(1) = size(self(c).data, 2) - flip_bbox(1)...
                                       - flip_bbox(3) + 1;
                    end
                    self_aug(t) = Limage(flip_data,...
                                         flip_gt_lmarks,...
                                         flip_m_lmarks,...
                                         flip_bbox,...
                                         labels,...
                                         fpath);
                    t = t + 1;
                    fprintf('Data augmentation - mirroring (%d/%d)\n', c, length(self))
                else
                    flip_data = fliplr(self(c).data);
                    flip_gt_lmarks = Limage.flip_lmarks(self(c).gt_lmarks,...
                                                     size(self(c).data));
                    flip_m_lmarks = {[]};
                    if ~any(cellfun(@isempty,self(c).m_lmarks))
                        for p = 1 : length(self(c).m_lmarks)
                            flip_m_lmarks{p} = Limage.flip_lmarks(self(c).m_lmarks{p},...
                                                  size(self(c).data));
                        end
                    end
                    flip_bbox = [];
                    if ~isempty(self(c).bbox)
                        flip_bbox = self(c).bbox;
                        flip_bbox(1) = size(self(c).data, 2) - flip_bbox(1)...
                                       - flip_bbox(3) + 1;
                    end
                    self_aug(t) = Limage(flip_data,...
                                     flip_gt_lmarks,...
                                     flip_m_lmarks,...
                                     flip_bbox);
                    t = t + 1;
                    fprintf('Data augmentation - mirroring (%d/%d)\n', c, length(self))
                end
            end
        end
        
        if ~isempty(rotate) && rotate~=0
            angle = [rotate -rotate];
            for r = 1 : length(angle)
                for c = 1 : length(self)
                    cstr = cellfun(@(x)strfind(self(c).image_fpath,x),...
                           {'L10','R10','L20','R20','L30','R30','L45','R45','L90','R90'},'un',0);
                    if any(~cellfun(@isempty,cstr))
                        rotmat = [ cosd(angle(r)) sind(angle(r)) 0;...
                                  -sind(angle(r)) cosd(angle(r)) 0;...
                                   0            0            1];
                        tform = affine2d(rotmat);
                        rot_gt_lmarks = [];
                        [rot_data, rot_gt_lmarks(:,2), rot_gt_lmarks(:,1)] = ...
                            io.transformImage(self(c).data,...
                                              self(c).gt_lmarks(:,2),...
                                              self(c).gt_lmarks(:,1), tform);
                        bg = mode(self(c).data(:));
                        rot_data(rot_data==0) = bg;
                        offset = mean(rot_gt_lmarks) - ...
                                 mean(self(c).gt_lmarks);
                        rot_m_lmarks = {[]};
                        if ~any(cellfun(@isempty,self(c).m_lmarks))
                            for p = 1 : length(self(c).m_lmarks)
                                rot_m_lmarks{p} = self(c).m_lmarks{p} + ...
                                    repmat(offset, size(self(c).m_lmarks{p},1), 1);
                            end
                        end
                        rot_bbox = [];
                        if ~isempty(self(c).bbox)
                            rot_bbox = [self(c).bbox(1:2) + offset,...
                                        self(c).bbox(3:4)];
                        end
                        fpath = [self(c).image_fpath(1:end-4),'_rot',...
                                 sprintf('%d',angle(r)),...
                                 self(c).image_fpath(end-3:end)];
                        self_aug(t) = Limage(rot_data,...
                                             rot_gt_lmarks,...
                                             rot_m_lmarks,...
                                             rot_bbox,...
                                             self(c).lmark_labels,...
                                             fpath);
                        t = t + 1;
                        fprintf('Data augmentation - rotation: %d%c (%d/%d)\n',...
                                 angle(r), char(176), c, length(self))
                    end
                end
            end
        end
    end
    
    function regain_original(self)
        for c = 1 : length(self)
            if isempty(self(c).image_fpath)
                error(['This object has no image data and', ...
                       'therefore original could not be recovered.'])
            end
            if isempty(self(c).lmark_fpath)
                error('This object has no landmark data.')
            end
            self(c).data = [];
            if isempty(self(c).orig_scale)
               scale = 1;
            else
               scale = self(c).orig_scale;
            end
            self(c).gt_lmarks = self(c).gt_lmarks./scale +...
                                self(c).crop_offset;
            self(c).bbox(1:2) = self(c).bbox(1:2)./scale +...
                                self(c).crop_offset(1,:);
            for p = 1 : length(self(c).m_lmarks)
                self(c).m_lmarks{p} = self(c).m_lmarks{p}./scale +...
                                      self(c).crop_offset;
            end
            self(c).crop_offset = zeros(size(self(c).gt_lmarks));
        end
    end
    
    function resize(self, varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addOptional(p, 'bbox_size', 128, @isnumeric)
        parse(p, self, varargin{:})
        self = p.Results.self;
        bbox_size = p.Results.bbox_size;
        
        for c = 1 : length(self)
            if isempty(self(c).bbox)
                error('Bounding box not defined for object %d.', c)
            end
            scale = bbox_size/self(c).bbox(3);
            self(c).data = imresize(self(c).data, scale);
            self(c).gt_lmarks = self(c).gt_lmarks .* scale;
            self(c).bbox = [self(c).bbox(1) * scale;...
                            self(c).bbox(2) * scale;...
                            bbox_size;...
                            bbox_size];
            for p = 1 : length(self(c).m_lmarks)
                self(c).m_lmarks{p} = self(c).m_lmarks{p} .* scale;
            end
            self(c).orig_scale = scale;
            if length(self) > 1
                fprintf('Resized to the bounding box size of %d (%d/%d)\n', bbox_size, c, length(self)) 
            end
        end
    end
    
    function normalize(self)
        for c = 1 : length(self)
            self(c).data = uint8(io.normalize8(mean(self(c).data,3)));
        end
    end
    
    function self = adjust_points_and_labels(self, labels, varargin)
    % Re-organize landmarks to match corresponding input labels. Excude data
    % with missing landmarks (all landmarks defined in labels should be included)
        if nargin > 2
            dmode = varargin{1};
        else
            dmode = 1;
        end
        if dmode==1 % sort lables of training images
            cout = [];
            for c = 1 : length(self)
                ix = ismember(self(c).lmark_labels,labels);
                if sum(ix)==length(labels)
                   [~,iloc] = ismember(labels,self(c).lmark_labels);
                   self(c).lmark_labels = self(c).lmark_labels(iloc(iloc~=0));
                   self(c).gt_lmarks = self(c).gt_lmarks(iloc(iloc~=0),:);
                   for d = 1 : length(self(c).m_lmarks)
                       if ~isempty(self(c).m_lmarks{d})
                           self(c).m_lmarks{d} = self(c).m_lmarks{d}(iloc(iloc~=0),:);
                       end
                   end
                   cout = [cout c];
                end
            end
            self = self(cout);
        else % sort/discard test image labels
            for c = 1 : length(self)
                [ix, loc] = ismember(self(c).lmark_labels, labels);
                for d = 1 : length(self(c).m_lmarks)
                    self(c).m_lmarks{d} = self(c).m_lmarks{d}(loc(loc~=0),:);
                end
                self(c).gt_lmarks = self(c).gt_lmarks(ix,:);
                self(c).lmark_labels = labels(loc(loc~=0));
            end
        end
    end
    
    function uint8gray(self)
        for c = 1 : length(self)
            if ~isa(self(c).data, 'uint8')
                [~, ~, ext] = fileparts(self(c).image_fpath);
                if any(strcmp(ext, {'.bnt','.abs'}))
                      tmp = self(c).data - nanmean(self(c).data(:)) + 128;
                      tmp = (tmp - min(tmp(:)))./((max(tmp(:))-min(tmp(:)))/255);
                      self(c).data = uint8(tmp);
                else
                    self(c).data = im2uint8(self(c).data);
                end
            end
            if size(self(c).data,3) == 3
                self(c).data = rgb2gray(self(c).data);
            end
            if length(self) > 1
                fprintf('Conversion to uint8 grayscale (%d/%d)\n', c, length(self))
            end
        end
    end
    
    function view(self,varargin)
        p = inputParser;
        addRequired(p, 'self',  @(x)isa(x, 'Limage'))
        addParameter(p, 'numbered', 0)
        addParameter(p, 'permuts', [])
        parse(p, self, varargin{:})
        self = p.Results.self;
        numbered = p.Results.numbered;
        permuts = p.Results.permuts;
        
        for c = 1 : length(self)
            imshow(self(c).data, [])
            hold on
            if ~isempty(self(c).bbox)
                rectangle('Position', [self(c).bbox(1),self(c).bbox(2),...
                    self(c).bbox(3),self(c).bbox(4)],'EdgeColor','red')
            end
            
%             if ~isempty(self(c).gt_lmarks)
%                 gt_lmark = self(c).gt_lmarks;
%                 if length(gt_lmark) == 68 && isempty(self(c).m_lmarks)
%                     plot(gt_lmark(1 :17,1), gt_lmark(1 :17,2), 'g',...
%                          gt_lmark(18:22,1), gt_lmark(18:22,2), 'g',...
%                          gt_lmark(23:27,1), gt_lmark(23:27,2), 'g',...
%                          gt_lmark(28:31,1), gt_lmark(28:31,2), 'g',...
%                          gt_lmark(32:36,1), gt_lmark(32:36,2), 'g',...
%                          gt_lmark([37:42,37],1), gt_lmark([37:42,37],2), 'g',...
%                          gt_lmark([43:48,43],1), gt_lmark([43:48,43],2), 'g',...
%                          gt_lmark(37:42,1), gt_lmark(37:42,2), 'g',...
%                          gt_lmark([49:60,49],1), gt_lmark([49:60,49],2), 'g',...
%                          gt_lmark([61:68,61],1), gt_lmark([61:68,61],2), 'g')
%                 end
%                 plot(gt_lmark(:,1), gt_lmark(:,2), 'g.', 'MarkerSize', 10)
%             end
            
            if ~cellfun(@isempty, self(c).m_lmarks)
                cmap = hsv(length(self(c).m_lmarks));
                if isempty(permuts)
                    npermuts = length(self(c).m_lmarks);
                else
                    npermuts = permuts;
                end
                for p = 1 : npermuts
                    m_lmark = self(c).m_lmarks{p};
                    if length(m_lmark) == 68
                        plot(m_lmark(1 :17,1), m_lmark(1 :17,2),...
                             m_lmark(18:22,1), m_lmark(18:22,2),...
                             m_lmark(23:27,1), m_lmark(23:27,2),...
                             m_lmark(28:31,1), m_lmark(28:31,2),...
                             m_lmark(32:36,1), m_lmark(32:36,2),...
                             m_lmark([37:42,37],1), m_lmark([37:42,37],2),...
                             m_lmark([43:48,43],1), m_lmark([43:48,43],2),...
                             m_lmark(37:42,1), m_lmark(37:42,2),...
                             m_lmark([49:60,49],1), m_lmark([49:60,49],2),...
                             m_lmark([61:68,61],1), m_lmark([61:68,61],2),...
                             'Color',cmap(p,:))
                    else
                        plot(m_lmark(:,1), m_lmark(:,2),...
                            '.','Color',cmap(p,:),'MarkerSize',10)
                    end
                end
            end
            if numbered
                if  ~isempty(self(c).lmark_labels)
                    if ischar(self(c).lmark_labels{1})
                        text(self(c).gt_lmarks(:,1), self(c).gt_lmarks(:,2),...
                            cellfun(@strsplit,self(c).lmark_labels,'un',0),...
                            'Color','c', 'HorizontalAlignment','right','FontSize',8);
                    else
                        text(self(c).gt_lmarks(:,1), self(c).gt_lmarks(:,2),...
                            self(c).lmark_labels,...
                            'Color','c', 'HorizontalAlignment','right');
                    end
                else
                    text(self(c).gt_lmarks(:,1), self(c).gt_lmarks(:,2),...
                    cellstr(num2str(colon(1,size(self(c).gt_lmarks,1))')),...
                    'Color','g');
                end
                if ~cellfun(@isempty, self(c).m_lmarks)
                   for p = 1 : npermuts
                       m_lmark = double(self(c).m_lmarks{p});
                       text(m_lmark(:,1), m_lmark(:,2),...
                           cellstr(num2str(colon(1,size(m_lmark,1))')),...
                           'Color','r');
                   end
                end
            end
            drawnow
            hold off
            if length(self) > 1
                fprintf('Show image %d/%d\n', c, length(self))
                pause
            end
        end
    end
    
    function avg_lmrk = landmark_average(self)
        avg_lmrk = mean(cat(3, self.gt_lmarks),3);
        if length(self) < 10
            warning('You need more annotations to compute good landmark average')
        end
    end
    
    function err = localization_err(self)
        err = NaN*ones(length(self),1);
        for c = 1 : length(self)
            if all(size(self(c).m_lmarks{1}) == size(self(c).gt_lmarks))
                err_tmp = sqrt(sum((self(c).gt_lmarks - self(c).m_lmarks{1}).^2, 2));
                err(c) = mean(err_tmp./self(c).orig_scale);
            elseif ~isempty(self(c).gt_lmarks)
                warning(['Test image #%d: The number of detected landmarks and the number',...
                         'of ground truth landmarks do not match.'],c)
            end
        end
    end
     
    function self_copy = copy(self)
        wid = 'MATLAB:structOnObject';
        warning('off',wid)
        for c = 1 : length(self)
            self_copy(c) = feval(class(self(c)));
            p = fieldnames(struct(self(c)));
            for i = 1:length(p)
                self_copy(c).(p{i}) = self(c).(p{i});
            end
        end
        warning('on',wid)
    end
    
    function data = bntread(self, varargin)
        if nargin < 2
            dmode = 'normals';
        else
            dmode = varargin{1};
        end
        [data3d, zmin, nrows, ncols, ~] = io.read_bntfile(self.image_fpath);
        data3d = reshape(data3d,ncols,nrows,5);
        x = data3d(:,:,1); y = data3d(:,:,2); z = data3d(:,:,3);
        nan_vals = z == zmin;
        x(nan_vals) = NaN; y(nan_vals) = NaN; z(nan_vals) = NaN;
        if strcmp(dmode,'depth')
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
        elseif strcmp(dmode,'shape-index')
            x = Limage.low_pass(x,30); 
            y = Limage.low_pass(y,30);
            z = Limage.low_pass(z,30);
            x = Limage.nanfilter(x,4,@nanmean);
            y = Limage.nanfilter(y,4,@nanmean);
            z = Limage.nanfilter(z,4,@nanmean);
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
            [~,~,p1,p2]=fd.surfature(xi,yi,data);
            data = 2/pi*atan((p1+p2)./(p1-p2));
        elseif strcmp(dmode,'camlight')
            h = figure;
            set(h, 'Visible', 'off')
            surf(x,y,z,'EdgeColor','none')
            view(0,90)
            colormap(gray)
            camlight('headlight')
            axis equal
            axis off
            f = getframe(gcf);
            close(h)
            data = f.cdata;
            data = im2uint8(rgb2gray(data));
            row_keep = find(~(sum(data,2)==240*size(data,2)),1,'first'):...
                      find(~(sum(data,2)==240*size(data,2)),1,'last');
            col_keep = find(~(sum(data,1)==240*size(data,1)),1,'first'):...
                      find(~(sum(data,1)==240*size(data,1)),1,'last');
            data = data(row_keep,col_keep);
            data = imresize(data, [round(max(y(:)))-min(y(:)) round(max(x(:))-min(x(:)))]);
        elseif strcmp(dmode,'normals')
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
            [~,~,data] = surfnorm(data);
        end
        data = single(data);
        self.data = data;
        
        lmrk = Limage.ptsread(self.lmark_fpath);
        iy = round(size(data,1) - lmrk(:,2) + min(y(:)));
        iy(iy<=0)=1; iy(iy>size(data,1))=size(data,1);
        ix = round(lmrk(:,1) - min(x(:)));
        ix(ix<=0)=1; ix(ix>size(data,2))=size(data,2);
        self.gt_lmarks = [ix iy];
        fprintf('Image loaded into memory (%s)\n', datestr(now,'HH:MM:SS'))
    end
    
    function data = absread(self, varargin)
        if nargin < 2
            dmode = 'normals';
        else
            dmode = varargin{1};
        end
        [x,y,z,fl] = io.absload(self.image_fpath);
        x(~fl) = NaN; y(~fl) = NaN; z(~fl) = NaN;
        if strcmp(dmode,'depth')
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            if 0
                clst = io.cluster_detect(z,'head_area',y);
                xt = x(~isnan(z) & clst==1);
                yt = y(~isnan(z) & clst==1);
                zt = z(~isnan(z) & clst==1);
            else
                xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            end
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
        elseif strcmp(dmode,'shape-index')
            x = Limage.low_pass(x,30); 
            y = Limage.low_pass(y,30);
            z = Limage.low_pass(z,30);
            x = Limage.nanfilter(x,4,@nanmean);
            y = Limage.nanfilter(y,4,@nanmean);
            z = Limage.nanfilter(z,4,@nanmean);
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
            [~,~,p1,p2]=fd.surfature(xi,yi,data);
            data = 2/pi*atan((p1+p2)./(p1-p2));
        elseif strcmp(dmode,'camlight')
            h = figure;
            set(h, 'Visible', 'off')
            surf(x,y,z,'EdgeColor','none')
            view(0,90)
            colormap(gray)
            camlight('headlight')
            axis equal
            axis off
            f = getframe(gcf);
            close(h)
            data = f.cdata;
            data = im2uint8(rgb2gray(data));
            row_keep = find(~(sum(data,2)==240*size(data,2)),1,'first'):...
                      find(~(sum(data,2)==240*size(data,2)),1,'last');
            col_keep = find(~(sum(data,1)==240*size(data,1)),1,'first'):...
                      find(~(sum(data,1)==240*size(data,1)),1,'last');
            data = data(row_keep,col_keep);
            data = imresize(data, [round(max(y(:)))-min(y(:)) round(max(x(:))-min(x(:)))]);
        elseif strcmp(dmode,'normals')
            [xi,yi] = meshgrid(min(x(:)):max(x(:)), max(y(:)):-1:min(y(:)));
            if 0
                clst = io.cluster_detect(z,'head_area',y);
                xt = x(~isnan(z) & clst==1);
                yt = y(~isnan(z) & clst==1);
                zt = z(~isnan(z) & clst==1);
            else
                xt = x(~isnan(z)); yt = y(~isnan(z)); zt = z(~isnan(z));
            end
            F = scatteredInterpolant(xt,yt,zt,'linear','none');
            data = F(xi,yi);
            [~,~,data] = surfnorm(data);
        end
        self.data = data;
        
        lmrk = Limage.ptsread(self.lmark_fpath)';
        lmrk = [lmrk(:,1)-nanmin(x(:))+1, size(data,1)-lmrk(:,2)+nanmin(y(:))+1];
        self.gt_lmarks = lmrk;
        fprintf('Image loaded into memory (%s)\n', datestr(now,'HH:MM:SS'))
    end
    
    function adapt_labels_to_Bosphorus_notation(self)
        for c = 1 : length(self)
            labels = self(c).lmark_labels;
            self(c).lmark_labels = [];
            for l = 1 : length(labels)
                switch labels{l}
                    case 1
                        self(c).lmark_labels{l,1} = 'Outer left eye corner';
                    case 2
                        self(c).lmark_labels{l,1} = 'Inner left eye corner';
                    case 3
                        self(c).lmark_labels{l,1} = 'Inner right eye corner';
                    case 4
                        self(c).lmark_labels{l,1} = 'Outer right eye corner';
                    case 5
                        self(c).lmark_labels{l,1} = 'Nose tip';
                    case 6
                        self(c).lmark_labels{l,1} = 'Left mouth corner';
                    case 7
                        self(c).lmark_labels{l,1} = 'Right mouth corner';
                    case 8
                        self(c).lmark_labels{l,1} = 'Chin middle';
                end
            end
        end
    end
    
    function self = discard_nan_rows_and_cols(self)
        for c = 1 : length(self)
            dat = self(c).data;
            row_first = find(nansum(dat,2),1,'first');
            row_last = find(nansum(dat,2),1,'last');
            col_first = find(nansum(dat,1),1,'first');
            col_last = find(nansum(dat,1),1,'last');
            self(c).data = dat(row_first : row_last,...
                               col_first : col_last);
            self(c).gt_lmarks = self(c).gt_lmarks -...
                repmat([col_first-1 row_first-1], size(self(c).gt_lmarks,1),1);
            for d = 1 : length(self(c).m_lmarks)
                if ~isempty(self(c).m_lmarks{d})
                    self(c).m_lmarks{d} = self(c).m_lmarks{d} -...
                        repmat([col_first-1 row_first-1], size(self(c).m_lmarks{d},1),1);
                end
            end
        end
    end

end % methods

methods(Static)
    
    function lmrk_out = flip_lmarks(lmrk, img_size)
        lmrk_out = [img_size(2)-lmrk(:,1)+1, lmrk(:,2)];
        if length(lmrk) == 68
            lmrk_out = lmrk_out([17:-1:1,27:-1:18,28:31,36:-1:32,46:-1:43,...
                48,47,40:-1:37,42,41,55:-1:49,60:-1:56,65,64:-1:61,68:-1:66],:);
        elseif length(lmrk) == 49
            lmrk_out = lmrk_out([10:-1:1,11:14,19:-1:15,29:-1:26,31,30,...
                23:-1:20,25,24,38:-1:32,43:-1:39,46:-1:44,49:-1:47],:);
        elseif length(lmrk) == 66
            lmrk_out = lmrk_out([17:-1:1,27:-1:18,28:31,36:-1:32,46:-1:43,...
                48,47,40:-1:37,42,41,55:-1:49,60:-1:56,63:-1:61,66:-1:64],:);
        elseif length(lmrk) == 70
            lmrk_out = lmrk_out([17:-1:1,27:-1:18,28:31,36:-1:32,46:-1:43,48,...
                47,40:-1:37,42,41,55:-1:49,60:-1:56,63:-1:61,66:-1:64,70:-1:67],:);
        elseif length(lmrk) == 72
            lmrk_out = lmrk_out([17:-1:1,27:-1:18,28:31,36:-1:32,46:-1:43,48,47,...
                40:-1:37,42,41,55:-1:49,60:-1:56,65,64:-1:61,68:-1:66,72:-1:69],:);
        else
            error('Check the number of landmarks.')
        end
        
    end
    
    function [lmarks, labels] = ptsread(fname)
        [~,name,ext] = fileparts(fname);
        labels = [];
        switch ext
            case '.pts'
                fid = fopen(fname);
                if fid==-1
                    error('Error occured while trying to read file %s',fname)
                end
                npts = textscan(fid,'%s %f',1,'HeaderLines',1);
                lmarks = textscan(fid,'%f %f',npts{2},...
                                 'MultipleDelimsAsOne',2,'Headerlines',2);
                lmarks = cell2mat(lmarks);
                fclose(fid);
            case '.lm2'
                fid = fopen(fname, 'rt');
                if (fid == -1)
                    error(['Could not open file, ' fname]);
                end
                fgetl(fid); fgetl(fid);
                N = fscanf(fid, '%d');
                fgetl(fid); fgetl(fid); fgetl(fid);
                labels = cell(N,1);
                for n=1:N
                    labels{n} = fgetl(fid);
                end
                fgetl(fid); fgetl(fid);
                lmarks = fscanf(fid, '%g %g', [2 inf])';
                fclose(fid);
            case '.lm3'
                if strcmp(name(end-7:end),'.abs.bin')
                    lmrk = importdata(fname);
                    labels = num2cell(uint8(lmrk.data(:,1)));
                    lmarks = lmrk.data(:,2:4)';
                else
                    fid = fopen(fname, 'rt');
                    if (fid == -1)
                        error(['Could not open file, ' fname]);
                    end
                    fgetl(fid);
                    if fgetl(fid) == '*'
                        while ( ~isempty(fgetl(fid)) )
                        end
                    end
                    N = fscanf(fid, '%d');
                    labels = cell(N,1);
                    lmarks = zeros(N,3);
                    fgetl(fid);
                    for n=1:N
                        labels{n} = fgetl(fid);
                        lmarks(n,:) = fscanf(fid, '%g', 3);
                        fgetl(fid);
                    end
                    fclose(fid);
                end
            case '.ldmk'
                lmarks = io.readLdmk(fname);
                labels = {lmarks.label};
                lmarks = cellfun(@str2double, [lmarks(:).position]);
        end
    end
    
    function  ptswrite(lmarks,fname)
        fid = fopen(fname,'w');
        fprintf(fid, 'version: 1\n');
        fprintf(fid, 'n_points:  %d\n',size(lmarks,2));
        fprintf(fid, '{\n');
        fprintf(fid,'%f %f\n',lmarks);
        fprintf(fid, '}');
        fclose(fid);
    end
    
    function data = video_buffer(vid_name, fnumber, fsampling)
        % Using persistent variables enables much faster video processing
        persistent vid_buf name_buf fcount_buf
        if isempty(vid_buf) || ~isequal(name_buf, vid_name)
            name_buf = vid_name;
            vid_obj = VideoReader(name_buf);
            vid_buf = vid_obj;
            fcount_buf = 0;
        end
        while fcount_buf < fnumber
           data = readFrame(vid_buf);
           if fcount_buf == 0 || isempty(fsampling)
               fcount_buf = fcount_buf + 1;
           else
               fcount_buf = fcount_buf + fsampling;
           end
        end
        if ~exist('data','var')
            clear vid_buf
            data = io.video_buffer(vid_name, fnumber, fsampling);
        end
    end

    function img = low_pass(img,t)
        img_tmp = diff(img);
        img_tmp = [img_tmp;NaN*zeros(1,size(img,2))];
        img_tmp(abs(img_tmp)>t) = NaN;
        img_tmp = filter2(fspecial('average',2),img_tmp);
        img_tmp(~isnan(img_tmp)) = 1;
        img_tmp(isnan(img_tmp)) = 0;
        img(~img_tmp) = NaN;
    end
    
    function y = nanfilter(x, s, fun)
        x = [NaN*ones(size(x,1),s), x, NaN*ones(size(x,1),s)];
        x = [NaN*ones(s,size(x,2)); x; NaN*ones(s,size(x,2))];
        x = colfilt(x, [s s], 'sliding', fun);
        y = x(s+1:end-s, s+1:end-s);
    end
    
end % static methods

end % classdef
  