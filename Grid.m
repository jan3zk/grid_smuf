classdef Grid < Lfitter
% Gated multiple RIdge Descent

properties(GetAccess = public, SetAccess = protected)
    pca_norm_bin
    mean_lmarks
    bb2lm
    reg_labels
    mean_gt
end

methods
    function self = Grid()
        self.feature_type = 'vhog';
        self.permuts = 15;
        self.lambda = linspace(1e3, 1e2, self.iters);
        self.block_size = round(linspace(40, 20, self.iters));
    end
    
    function train(self, limg)
        
        p = inputParser;
        addRequired(p, 'self', @(x)isa(x, 'Lfitter'))
        addRequired(p, 'limg', @(x)isa(x, 'Limage'))
        parse(p, self, limg)
        self = p.Results.self;
        limg = p.Results.limg;
        
        if length(self.lambda) == 1
            self.lambda = self.lambda*ones(self.iters, 1);
        end
        
        for r = 1 : 5
            
            switch r
                case 1
                    fprintf('\nTrain frontal model...\n')
                    limgr = limg(cellfun(@isempty,strfind({limg.image_fpath},'R30')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'L30')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'R45')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'L45')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'45R')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'45L')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'60R')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'60L')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'R90')) &...
                                 cellfun(@isempty,strfind({limg.image_fpath},'L90')));
                    self.reg_labels{r} = self.labels; %bosphorus
                    %self.reg_labels{r} = labels([7:10,14,16,21:22]); %und
                case 2
                    fprintf('\nTrain model facing left...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'L30'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'L45'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'45L\'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'60L'))==1);
                    self.reg_labels{r} = self.labels([1:3 7:8 11 13:14 16:20 22]); %bosphorus
                    %self.reg_labels{r} = labels([7:8,14,16,22]); %und
                case 3
                    fprintf('\nTrain model facing right...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'R30'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'R45'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'45R\'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'60R'))==1);
                    self.reg_labels{r} = self.labels([4:6 9:10 12 14:15 17:22]); %bosphorus
                    %self.reg_labels{r} = labels([9:10,14,21,22]); %und
                case 4
                    fprintf('\nTrain left profile model...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'L90'))==1);
                    self.reg_labels{r} = self.labels([1:3 7 11 13:14 16:20 22]); %bosphorus
                    %self.reg_labels{r} = labels([7:8,14,16,22]); %und
                case 5
                    fprintf('\nTrain right profile model...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'R90'))==1);
                    self.reg_labels{r} = self.labels([4:6 10 12 14:15 17:22]); %bosphorus
                    %self.reg_labels{r} = labels([9:10,14,21,22]); %und
            end
            
            limgr = limgr.adjust_points_and_labels(self.reg_labels{r});
            self.bb2lm{r} = limgr.lmarks_from_bb();
            if self.permuts > 1
                limgr.random_sampling(self.bb2lm{r}, self.permuts)
            end
            
            if self.verbose
                rng('shuffle')
                ridx = randi(length(limgr));
                fh = figure;
                set(fh, 'Name', 'Initial state of a random training image')
                limgr(ridx).view('permuts', self.permuts)
                fh = figure('units','normalized','outerposition',[0 0 1 1]);
                set(fh,'Name','Point convergence on a random training image') 
            end

            % Matrix of ground truth locations
            gt_lmrk = zeros(length(limgr(1).gt_lmarks(:)),...
                            self.permuts*length(limgr));
            for c = 1 : length(limgr)
                for p = 1 : self.permuts
                    gt_lmrk(:, (c-1)*self.permuts+p) = limgr(c).gt_lmarks(:);
                end
            end
            
            self.mean_gt{r} = mean(extract_feat(self, limgr, 'lmark','gt', 'merge','global', 'norm',0), 2);
            
            for i = 1 : self.iters

                fprintf('\nTrain landmarker (iteration %d/%d)\n', i, self.iters)

                % Get matrix of update locations from training data
                m_lmrk = zeros(size(gt_lmrk));
                for c = 1 : length(limgr)
                    for p = 1 : self.permuts
                        m_lmrk(:, (c-1)*self.permuts+p) = limgr(c).m_lmarks{p}(:);
                    end
                end

                % Extract features
                fprintf('Computing features ...\n')
                [X, complete] = extract_feat(self, limgr, 'trn', 1, 'iter', i, 'reg',r, 'merge','global', 'norm',0);
                X = X - repmat(self.mean_gt{r}, 1, size(X,2));
                
                % Location difference
                dY = gt_lmrk(:, complete) - m_lmrk(:, complete);
                self.mean_loc{r,i} = mean(dY, 2);
                dY = dY - repmat(self.mean_loc{r,i}, 1, size(dY, 2));

                % Learn regressor
                self.reg{r,i} = dY*...
                X'/(X*X' + self.lambda(i)*eye(size(X, 1)));
                
                % Update landmarks with the trained regressor
                m_lmrk(:, complete) = m_lmrk(:, complete) + self.reg{r,i}*X + ...
                         repmat(self.mean_loc{r,i}, 1, size(dY, 2));

                % Localization error
                err = mean(sqrt(mean((gt_lmrk(:, complete)-...
                                      m_lmrk(:, complete)).^2, 2)));
                fprintf('Localization error: %f\n', err)

                % Write updated locations to training data
                for c = 1 : length(limgr)
                    for p = 1 : self.permuts
                        limgr(c).m_lmarks{p} = ...
                            reshape(m_lmrk(:, (c-1)*self.permuts+p), [], 2);
                    end
                end

                if self.verbose
                    sh = subplot(2, ceil(self.iters/2), i);
                    limgr(ridx).view('permuts', self.permuts)
                    title(sh, sprintf('State after iteration %d', i))
                    drawnow
                end

            end
        
        end
        
    end

    function [loc_err, sel_err] = localize(self, limg)
        
        if self.verbose
            fh = figure('units','normalized','outerposition',[0 0 1 1]);
            set(fh, 'Name', 'Localize landmarks using trained landmarker')
        end
        
        cr = zeros(1,length(limg));
        tm = [];
        for c = 1 : length(limg)
            
            if self.verbose
                sh = subplot(2, ceil((self.iters+1)/2), 1);
                limg(c).view()
                title(sh, 'Face detection')
                drawnow
            end
            
            if length(limg) > 1
                fprintf('Landmark localization %d/%d\n', c, length(limg))
            end

            for i = 1 : self.iters
                
                if i == 1
                    mahal_dist = zeros(5,self.permuts);
                    for r = 1 : 5
                        limg(c).lmarks_from_bb(self.bb2lm{r})
                        limg(c).random_sampling(self.bb2lm{r}, self.permuts)
                        [Xm, ~] = extract_feat(self, limg(c), 'iter', i, 'reg',r, 'permut',self.permuts, 'norm',0, 'merge','global');
                        S = 1./(self.std_feat{r,i}.^2);
                        mu = self.mean_feat{r,i};
                        for p = 1 : self.permuts
                            md = (Xm(:,p)-mu)'*diag(S)*(Xm(:,p)-mu); %Xm(:,p)'*Xm(:,p)
                            mahal_dist(r,p) = 1/length(self.reg_labels{r})*sqrt(md);
                        end
                    end
                    [~,cr(c)] = min(min(mahal_dist,[],2));
                    limg(c).lmarks_from_bb(self.bb2lm{cr(c)})
                end
                [X, complete] = extract_feat(self, limg(c), 'iter', i, 'reg',cr(c), 'merge','global', 'norm',0);
                if size(X,1)==length(self.mean_gt{cr(c)})
                    X = X - self.mean_gt{cr(c)};
                end
                if all(complete)
                   %tic 
                    limg(c).m_lmarks{1} = reshape(...
                        limg(c).m_lmarks{1}(:) + ...
                        self.reg{cr(c),i}*X + ...
                        self.mean_loc{cr(c),i},...
                                       size(limg(c).m_lmarks{1}, 1), 2);
                   %tm = [tm toc];
                else
                    fprintf(['Locations (obj %d, iter %d) aren''t updated, ', ...
                            'since some of the features couldn''t be extracted.\n'], c, i)
                end
                if self.verbose
                    sh = subplot(2, ceil((self.iters+1)/2), i+1);
                    limg(c).view()
                    title(sh, sprintf('Iteration %d', i))
                    drawnow
                end

            end
            
            if self.verbose
                pause
            end
            
        end
        %fprintf('Average time: %f\n', mean(tm))
        
        % Check for model selection errors
        if all(~cellfun(@isempty,{limg.image_fpath}))
            sel_err = ...
            ( (cellfun(@length,strfind({limg.image_fpath},'L10')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'L20')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'L30')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'45L')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'60L')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 ) &...
              (cr == 3 | cr == 5) ) |...
            ( (cellfun(@length,strfind({limg.image_fpath},'R10')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R20')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R30')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'45R')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'60R')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R90')) == 1 ) &...
              (cr == 2 | cr == 4) ) |...
            ( (cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'45L')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'45R')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'60L')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'60R')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 |...
               cellfun(@length,strfind({limg.image_fpath},'R90')) == 1 ) &...
               cr == 1 ) |...
            ( ~(cellfun(@length,strfind({limg.image_fpath},'L20')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'L30')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'45L')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'60L')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'R20')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'R30')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'45R')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'60R')) == 1 |...
                cellfun(@length,strfind({limg.image_fpath},'R90')) == 1) &...
               (cr == 4 | cr == 5) );
            sel_err = sum(sel_err)/length(limg)*100;
        end
        
        % Localization error
        loc_err = NaN*ones(length(self.labels),length(limg));
        for c = 1 : length(limg)
            if ~isempty(limg(c).m_lmarks{1}) && ~isempty(limg(c).gt_lmarks)
                limg(c).adjust_points_and_labels(self.reg_labels{cr(c)},2);
                err_tmp = sqrt(sum((limg(c).gt_lmarks - limg(c).m_lmarks{1}).^2, 2));
                [~,idx] = ismember(limg(c).lmark_labels,self.labels);
                loc_err(idx,c) = err_tmp./limg(c).orig_scale;
            end
        end
        loc_err = [nanmean(loc_err,2) nanstd(loc_err,0,2)];
        loc_err = nanmean(loc_err);
    end
    
end

end