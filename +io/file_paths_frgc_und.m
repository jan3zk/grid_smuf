function [imname_train, lmname_train, imname_test, lmname_test] = file_paths_frgc_und(varargin)
p = inputParser;
addParameter(p, 'dmode', 'Creusot', @ischar)
addParameter(p, 'folder', {'DB45L'}, @iscell) %{'DB00F_mild','DB45L','DB45R','DB60R','DB60L'}
parse(p, varargin{:})
dmode = p.Results.dmode;
folders = p.Results.folder;

dbdirs = {'E:\Databases\FRGC_2.0\BEE_DIST\data\orig\nd1\range\',...
          'E:\Databases\HID-data-F\',...
          'E:\Databases\HID-data-G\'};
      
if strcmp(dmode,'Creusot')
    
    fileID = fopen('data\FRGC-results\list-test.txt');
    names = textscan(fileID,'%s');
    fclose(fileID);
    names = names{1};
    c2 = 1;
    for c = 1 : length(names)
        fpath = [dbdirs{1},names{c},'.abs'];
        if exist(fpath,'file') == 2
            imname_test{c2,1} = fpath;
            lmname_test{c2,1} = ['data\FRGC-results\test\' names{c} '.groundtruth.ldmk'];
            c2 = c2 + 1;
        end
    end
    
    fileID = fopen('data\FRGC-results\list-train.txt');
    names = textscan(fileID,'%s');
    fclose(fileID);
    names = names{1};
    c2 = 1;
    for c = 1 : length(names)
        fpath = [dbdirs{1},names{c},'.abs'];
        if exist(fpath,'file') == 2
            imname_train{c2,1} = fpath;
            lmname_train{c2,1} = ['data\FRGC-results\train\' names{c} '.groundtruth.ldmk'];
            c2 = c2 + 1;
        end
    end
    
elseif strcmp(dmode,'Perakis')

    c4 = 1;
    for c1 = 1 : length(folders)
        names = cellstr(ls('data\facial-landmarks\DB_TRAIN\*.lm3'));
        names = regexprep(names,'.abs.bin.lm3','');
        for c2 = 1 : length(names)
            for c3 = 1 : length(dbdirs)
                fpath = [dbdirs{c3} names{c2} '.abs'];
                if exist(fpath,'file') == 2
                    imname_train{c4,1} = fpath;
                    lmname_train{c4,1} = ['data\facial-landmarks\DB_TRAIN\',names{c2},'.abs.bin.lm3'];
                    c4 = c4 + 1;
                    break
                end
            end
        end
    end
    
    c4 = 1;
    for c1 = 1 : length(folders)
        names = cellstr(ls(['data\facial-landmarks\',folders{c1},'\*.lm3']));
        names = regexprep(names,'.abs.bin.lm3','');
        for c2 = 1 : length(names)
            for c3 = 1 : length(dbdirs)
                fpath = [dbdirs{c3} names{c2} '.abs'];
                if exist(fpath,'file') == 2
                    imname_test{c4,1} = fpath;
                    lmname_test{c4,1} = ['data\facial-landmarks\',folders{c1},'\',names{c2},'.abs.bin.lm3'];
                    c4 = c4 + 1;
                    break
                end
            end
        end
    end
end
