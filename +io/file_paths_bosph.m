function [imname_3D, lmname_3D, imname_2D, lmname_2D] = file_paths_bosph(varargin)

p = inputParser;
addParameter(p, 'dbdir', 'E:\Databases\Bosphorus\BosphorusDB\', @ischar)
addParameter(p, 'filt', 0, @isnumeric)
parse(p, varargin{:})
dbdir = p.Results.dbdir;
filt = p.Results.filt;

d = dir(dbdir);
isub = [d(:).isdir];
imfolds = {d(isub).name}';
imfolds(ismember(imfolds,{'.','..'})) = [];

if ~filt
    imname_3D = [];
    imname_2D = [];
    lmname_3D = [];
    lmname_2D = [];
    for c = 1 : length(imfolds)
        tdir = []; imext = []; lmext = [];
        tname = cellstr(ls([dbdir, imfolds{c},'\*.bnt']));
        tname = regexprep(tname,'.bnt','');
        [imext{1:length(tname),1}] = deal('.bnt');
        [lmext{1:length(tname),1}] = deal('.lm3');
        [tdir{1:length(tname),1}] = deal([dbdir imfolds{c} '\']);
        imname_3D = [imname_3D; strcat(tdir, tname, imext)];
        lmname_3D = [lmname_3D; strcat(tdir, tname, lmext)];
        
        tname = cellstr(ls([dbdir, imfolds{c},'\*.png']));
        tname = regexprep(tname,'.png','');
        [imext{1:length(tname),1}] = deal('.png');
        [lmext{1:length(tname),1}] = deal('.lm2');
        [tdir{1:length(tname),1}] = deal([dbdir imfolds{c} '\']);
        imname_2D = [imname_2D; strcat(tdir, tname, imext)];
        lmname_2D = [lmname_2D; strcat(tdir, tname, lmext)];
    end
else
    labels = [{'Outer left eyebrow'};...
              {'Middle left eyebrow'};...
              {'Inner left eyebrow'};...
              {'Inner right eyebrow'};...
              {'Middle right eyebrow'};...
              {'Outer right eyebrow'};...
              {'Outer left eye corner'};...
              {'Inner left eye corner'};...
              {'Inner right eye corner'};...
              {'Outer right eye corner'};...
              {'Nose saddle left'};...
              {'Nose saddle right'};...
              {'Left nose peak'};... 
              {'Nose tip'};...
              {'Right nose peak'};...
              {'Left mouth corner'};...
              {'Upper lip outer middle'};...
              {'Right mouth corner'};...
              {'Upper lip inner middle'};...
              {'Lower lip inner middle'};...
              {'Lower lip outer middle'};...
              {'Chin middle'}];
    imname_3D = []; imname_2D = []; lmname_3D = []; lmname_2D = []; i = 1;
    for c = 1 : length(imfolds)
        tnames_3D = cellstr(ls([dbdir, imfolds{c},'\*.bnt']));
        tnames_3D = regexprep(tnames_3D,'.bnt','');
        tnames_2D = cellstr(ls([dbdir, imfolds{c},'\*.png']));
        tnames_2D = regexprep(tnames_2D,'.png','');
        for d = 1 : length(tnames_3D)
            tname_3D = [dbdir imfolds{c} '\' tnames_3D{d}];
            tname_2D = [dbdir imfolds{c} '\' tnames_2D{d}];
            [~, labl_3D] = read_lm3file([tname_3D '.lm3']);
            [~, labl_2D] = read_lm2file([tname_2D '.lm2']);
            if isequal(labels, labl_3D) && isequal(labels, labl_2D)
                imname_3D{i,1} = [tname_3D '.bnt'];
                lmname_3D{i,1} = [tname_3D '.lm3'];
                imname_2D{i,1} = [tname_2D '.png'];
                lmname_2D{i,1} = [tname_2D '.lm2'];
                i = i + 1;
            end
        end
    end
end
end

function [pts3D, labels] = read_lm3file(filepath)
fid = fopen(filepath, 'rt');
if (fid == -1)
    error(['Could not open file, ' filepath]);
end
fgetl(fid);
if fgetl(fid) == '*'
    while ( ~isempty(fgetl(fid)) )
    end
end
N = fscanf(fid, '%d');
labels = cell(N,1);
pts3D = zeros(N,3);
fgetl(fid);
for n=1:N
    labels{n} = fgetl(fid);
    pts3D(n,:) = fscanf(fid, '%g', 3);
    fgetl(fid);
end
fclose(fid);
end

function [pts2D, labels] = read_lm2file(filepath)
fid = fopen(filepath, 'rt');
if (fid == -1)
    error(['Could not open file, ' filepath]);
end
fgetl(fid); fgetl(fid);
N = fscanf(fid, '%d');
fgetl(fid); fgetl(fid); fgetl(fid);
labels = cell(N,1);
for n=1:N
    labels{n} = fgetl(fid);
end
fgetl(fid); fgetl(fid);
pts2D = fscanf(fid, '%g %g', [2 inf]);
fclose(fid);
end