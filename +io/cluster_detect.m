function idx = cluster_detect(depth, varargin)
p = inputParser;
addRequired(p, 'depth', @isnumeric)
addOptional(p, 'cmode', 'keep_all', @ischar)
addOptional(p, 'ydata', [], @(x) (isempty(x)|isnumeric(x)))
parse(p, depth, varargin{:})
depth = p.Results.depth;
cmode = p.Results.cmode;
y = p.Results.ydata;

if strcmp(cmode, 'head_area')
    warning('off','stats:kmeans:FailedToConverge')
    warning('off','stats:kmeans:MissingDataRemoved')
    if isempty(y)
        [~,y] = meshgrid(1:size(depth,2), size(depth,1):-1:1);
    end
    [idx,~] = kmeans(depth(:), 2, 'Start', 'cluster');
    if mean(y(idx==1)) > mean(y(idx==2)) && sum(idx(:)==1)> 5000
        idx = idx == 1;
    else
        idx = idx == 2;
    end
    idx(idx ~= 1) = 0;
    idx = reshape(idx,size(depth,1),size(depth,2));
    idx = bwtwolargestblob(idx,4);
elseif strcmp(cmode, 'keep_all')
    idx = im2bw(depth-min(depth(:)),0);
end
se90 = strel('line', 3, 90);
se0 = strel('line', 3, 0);
idx = imdilate(idx, [se90 se0]);
idx = imfill(idx, 'holes');


function [outim] = bwtwolargestblob(im,connectivity)
%
% 'bwlargestblob' reads in a 2-d binary image and outputs a binary image, retaining only the largest blob.
% 
% Usage: [outim] = bwlargestblob(im,connectivity)
% 
% im - 2-d binary image 
% conenctivity - Accepts 4/8 connectivity
% outim - Output binary image (with 1s and 0s)
% 
% Example:
% 
% im = imread('text.png');
% outim = bwlargestblob(im,8);
% figure; 
% subplot(1,2,1); imshow(im);
% subplot(1,2,2); imshow(255*outim);

if size(im,3)>1,
    error('bwlargestblob accepts only 2 dimensional images');
end

[imlabel totalLabels] = bwlabel(im,connectivity);
sizeblob = zeros(1,totalLabels);
for i=1:totalLabels,
    sizeblob(i) = length(find(imlabel==i));
end
[maxno largestBlobNo] = sort(sizeblob,'descend');

for c = 1 : 2
    outim_ = zeros(size(im),'uint8');
    outim_(imlabel==largestBlobNo(c)) = 1;
    if max(sum(outim_,2)) < max(sum(outim_,1))
       outim = outim_; 
       return;
    end
end
if ~exist('outim','var')
    outim_ = zeros(size(im),'uint8');
    outim_(imlabel==largestBlobNo(1)) = 1; 
    outim = outim_;
end