function [image_names, lmrk_names] = file_paths_300vw(varargin)

p = inputParser;
addOptional(p, 'retain_only_nth_frame', 10, @isnumeric)
parse(p, varargin{:})
nth_frame = p.Results.retain_only_nth_frame;

vw_dir = 'E:\Databases\Landmarks\300vw\';

% Generate subsampled videos by retaining only each N-th frame
d = dir(vw_dir);
isub = [d(:).isdir];
vid_folds = {d(isub).name}';
vid_folds(ismember(vid_folds,{'.','..'})) = [];
vid_folds = vid_folds(1:end-1); %exclude extra folder

for c = 1 : length(vid_folds)
    if ~exist([vw_dir vid_folds{c} sprintf('\\vid%s.avi',num2str(nth_frame))],'file')
        vid_orig = VideoReader([vw_dir vid_folds{c} '\vid.avi']);
        vid_samp = VideoWriter([vw_dir vid_folds{c} sprintf('\\vid%s.avi',num2str(nth_frame))]);
        vid_samp.FrameRate = vid_orig.FrameRate/nth_frame;
        open(vid_samp)
        t = 0;
        while hasFrame(vid_orig)
            frame = readFrame(vid_orig); t = t + 1; %disp(t)
            writeVideo(vid_samp, frame)
            for c2 = 1 : nth_frame-1
                if hasFrame(vid_orig)
                    readFrame(vid_orig); t = t + 1; %disp(t)
                else
                    break
                end
            end
        end
        close(vid_samp)
        clear vid_samp
        fprintf('Finished resampling video %s (%d/%d).\n',vid_folds{c}, c, length(vid_folds)-1)
    end
end

t = 1;
for c = 1 : length(vid_folds)
    frame_idx = cellstr(ls([vw_dir vid_folds{c} '\annot\*.pts']));
    for c2 = 1  : nth_frame : length(frame_idx)
        image_names{t} = [vw_dir vid_folds{c} sprintf('\\vid%s.avi',num2str(nth_frame))];
        lmrk_names{t} = [vw_dir vid_folds{c} '\annot\' frame_idx{c2}];
        t = t + 1;
    end
end
end