% Preprocessing of images from Bosphorus data set
function preproc_bosph(varargin)

    p = inputParser;
    addParameter(p, 'verbose', 1, @isnumeric)
    addParameter(p, 'savem', 0, @isnumeric)
    parse(p, varargin{:})
    verb = p.Results.verbose;
    savem = p.Results.savem;

    addpath('E:\Databases\Bosphorus\fileread')

    % Change these directories accordingly
    save_dir = 'C:\Users\janezk\Documents\MATLAB\SDM\learned_binary_features\data\metadata\'; %directory where preprocessed images are saved
    db_dir = 'E:\Databases\Bosphorus\BosphorusDB\'; %directory including raw image data from Bosphorus data set

    if verb, figure; end

    d = dir(db_dir);
    isub = [d(:).isdir];
    nameFolds = {d(isub).name}';
    nameFolds(ismember(nameFolds,{'.','..'})) = [];

    % Used points (22, all except ear points)
    labels0 = [{'Outer left eyebrow'};...
               {'Middle left eyebrow'};...
               {'Inner left eyebrow'};...
               {'Inner right eyebrow'};...
               {'Middle right eyebrow'};...
               {'Outer right eyebrow'};...
               {'Outer left eye corner'};...
               {'Inner left eye corner'};...
               {'Inner right eye corner'};...
               {'Outer right eye corner'};...
               {'Nose saddle left'};...
               {'Nose saddle right'};...
               {'Left nose peak'};... 
               {'Nose tip'};...
               {'Right nose peak'};...
               {'Left mouth corner'};...
               {'Upper lip outer middle'};...
               {'Right mouth corner'};...
               {'Upper lip inner middle'};...
               {'Lower lip inner middle'};...
               {'Lower lip outer middle'};...
               {'Chin middle'}];

    selectedPoseTypes = {'E', 'N', 'CAU', 'LFAU', 'UFAU'};
    t = 1;
    for t1 = 1 : length(nameFolds) % for each ID
        names3D = cellstr(ls([db_dir,nameFolds{t1},'\*.lm3']));
        names2D = cellstr(ls([db_dir,nameFolds{t1},'\*.png']));
        for t2 = 1 : length(names3D) % for each photo of ID
            [pts3D, labels] = read_lm3file([db_dir,nameFolds{t1},'\',names3D{t2}]);
            ix = ismember(labels,labels0);
    %         if sum(ix) == 22 
                info = strsplit(names3D{t2}, '_');
    %             if sum(strcmp(info{2},selectedPoseTypes)) >= 1

                    labels = labels(ix);
                    pts3D = pts3D(:,ix);
                    idt = names3D{t2}; 
                    bos(t).fpl = labels;
                    bos(t).id = str2double(idt(3:5));
                    bos(t).rec = idt(7:end-4);

                    [data3d, zmin, nrows, ncols, ~] = read_bntfile([db_dir,nameFolds{t1},'\',idt(1:end-4),'.bnt']);
                    data2d = imread([db_dir,nameFolds{t1},'\',idt(1:end-4),'.png']);

                    data3d = reshape(data3d,ncols,nrows,5);

                    asx = data3d(:,:,1);
                    asy = data3d(:,:,2);
                    asz = data3d(:,:,3);

                    nan_vals = asz == zmin;
                    asx(nan_vals) = NaN;
                    asy(nan_vals) = NaN;
                    asz(nan_vals) = NaN;

                    asx = low_pass(asx,30);
                    asy = low_pass(asy,30);
                    asz = low_pass(asz,30);

                    asx = nanfilter(asx,4,@nanmean);
                    asy = nanfilter(asy,4,@nanmean);
                    asz = nanfilter(asz,4,@nanmean);

                    [xi,yi] = meshgrid(min(asx(:)):1:max(asx(:)), min(asy(:)):1:max(asy(:)));
                    xt = asx(~isnan(asz)); yt = asy(~isnan(asz)); zt = asz(~isnan(asz));
                    F = scatteredInterpolant(xt,yt,zt,'linear','none');
                    I3d = F(xi,yi);
                    I3d = single(I3d);
                    I2d = flipud(imresize(data2d,[size(I3d,1),size(I3d,2)]));
                    bos(t).imsize = size(I3d);

                    % Transfrom anotated fiducial points
                    bos(t).fp = [pts3D(1,:) - min(asx(:));...
                                 pts3D(2,:) - min(asy(:))];

                    if verb
                        sh = surf(I3d,'EdgeColor','none');
                        hold on; view(0,90); colormap(gray); camlight('headlight'); axis equal; axis off
                        set(sh,'cdata',I2d); sh.FaceLighting = 'gouraud'; sh.AmbientStrength = 0.8;
                        subx = round(bos(t).fp(2,:)); subx(subx<1) = 1; subx(subx>size(I3d,1)) = size(I3d,1);
                        suby = round(bos(t).fp(1,:)); suby(suby<1) = 1; suby(suby>size(I3d,2)) = size(I3d,2); 
                        plot3(bos(t).fp(1,:),bos(t).fp(2,:),I3d(sub2ind(size(I3d), round(bos(t).fp(2,:)), round(bos(t).fp(1,:))))+1,'g.')
                        hold off; pause
                        drawnow
                    end

                    save([save_dir,sprintf('%03d_%s.mat',bos(t).id,bos(t).rec)],'I3d')
                    save([save_dir,sprintf('%03d_%s_c.mat',bos(t).id,bos(t).rec)],'I2d')
                    ptswrite(bos(t).fp,[save_dir,sprintf('%03d_%s.pts',bos(t).id,bos(t).rec)])

                    fprintf('Processed image #%d.\n',t)

                    t = t + 1;
    %             end

    %         end
        end
    end

    if savem
       save([save_dir,'bosph_metadata_22pts.mat'],'bos')
    end
end

%% Subfunctions

function I = low_pass(I,t)
    I1 = diff(I);
    I1 = [I1;NaN*zeros(1,size(I,2))];
    I1(abs(I1)>t) = NaN;
    I1 = filter2(fspecial('average',2),I1);
    I1(~isnan(I1)) = 1;
    I1(isnan(I1)) = 0;
    I(~I1) = NaN;
end

function y=nanfilter(x,s,fun)
    x=[NaN*ones(size(x,1),s),x,NaN*ones(size(x,1),s)];
    x=[NaN*ones(s,size(x,2));x;NaN*ones(s,size(x,2))];
    x=colfilt(x,[s s],'sliding',fun);
    y=x(s+1:end-s,s+1:end-s);
end

function ptswrite(pts,fname)
    fid = fopen(fname,'w');
    fprintf(fid, 'version: 1\n');
    fprintf(fid, 'n_points:  %d\n',size(pts,2));
    fprintf(fid, '{\n');
    fprintf(fid,'%f %f\n',pts);
    fprintf(fid, '}');
    fclose(fid);
end

function pts = ptsread(fname)
    fid = fopen(fname);
    if fid==-1
        error('Napaka pri branju datoteke %s',fname)
    end
    npts = textscan(fid,'%s %f',1,'HeaderLines',1);
    pts = textscan(fid,'%f %f',npts{2},'MultipleDelimsAsOne',2,'Headerlines',2);
    pts = cell2mat(pts);
    fclose(fid);
end