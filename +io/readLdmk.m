function landmarks = readLdmk(file)
% readLdmk: function to read Clement Creusot's '.ldmk' files
%   
%   The files contain manually labelled 3D landmark positions
%
%   Input: 
%       the landmark filename (.ldmk)
%   Outputs:
%       nx3 array of landmarks 
%       nx1 array of labels
%
%   
  fid = fopen(file,'r');
  if fid==-1
     error('Napaka pri branju landmarkov.') %TODO
  end
  next=1;

  landmarks = struct('position',[],'label',-1);

  while ~feof(fid) % go until EOF
    line = fgetl(fid);
    [r] = regexp(line,'[^\ ]+','match');
    if strcmp(r(1),'#')==1
	  % comments
    else
      nb = str2double(char(r(1)));
      if isnan(nb) 
        error('Napaka pri branju landmarkov.') % TODO
      end
      for i=1:nb
        rank = 2+(i-1)*2;
        if strcmp(r(rank),'position')==1	 
          lpos = regexp(char(r(rank+1)),'[^,]+','match'); % works for 2D,3D ...
              landmarks(next).position = [lpos(1,1) ; lpos(1,2) ; lpos(1,3) ];
        end
        if strcmp(r(rank),'label')==1	 
          landmarks(next).label = str2double(char(r(rank+1))); 
        end
      end
      next = next +1;
    end
  end
  
  fclose(fid);
