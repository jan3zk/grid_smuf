for c = 1 : length(trn)
    load([db_dir,sprintf('%03d_%s.mat',trn(c).id,strrep(trn(c).rec,'_mirrored',''))])
    if strfind(trn(c).rec, 'mirrored')
        I3d = fliplr(I3d);
    end
   [nx, ny, Is] = surfnorm(I3d);
   Is = im2uint8(Is);
   limg(c) = Limage(Is,round(trn(c).fp'),{round(trn(c).fp0')});
end

for c = 1 : length(bost)
    load([db_dir,sprintf('%03d_%s.mat',bost(c).id,strrep(bost(c).rec,'_mirrored',''))])
    if strfind(bost(c).rec, 'mirrored')
        I3d = fliplr(I3d);
    end
   [nx, ny, Is] = surfnorm(I3d);
   Is = im2uint8(Is);
   limg_test(c) = Limage(Is,round(bost(c).fp'),{round(bost(c).fp0{1}')});
end