function [image_names, lmrk_names] = file_paths_frgc()
load('C:\Users\janezk\Documents\MATLAB\SDM\learned_binary_features\+io\FRGCv2_metadata.mat','uri','emotiontype')
uri = cellstr(uri);
emotiontype = cellstr(emotiontype);
emotiontype = emotiontype(~cellfun(@isempty,regexp(uri,'ppm$')));
uri = uri(~cellfun(@isempty,regexp(uri,'ppm$')));
uri = cellfun(@(x) x, regexp(uri,'(?<=/).*(?=....)','match'));
uri = uri(cellfun(@isempty,(strfind(emotiontype,'BlankStare'))));
emotiontype = emotiontype(cellfun(@isempty,(strfind(emotiontype,'BlankStare'))));
uri = uri(cellfun(@isempty,(strfind(emotiontype,'Other'))));
emotiontype = emotiontype(cellfun(@isempty,(strfind(emotiontype,'Other'))));
img_dir = 'E:\Databases\FRGC_2.0\BEE_DIST\data\orig\nd1\range\';
lmrk_dir = 'E:\Databases\Landmarks\frgc\';
image_names = io.cellstrhcat([repmat({img_dir},length(uri),1),...
                             uri, repmat({'.ppm'},length(uri),1)]);
lmrk_names = io.cellstrhcat([repmat({lmrk_dir},length(uri),1),...
                            uri, repmat({'.pts'},length(uri),1)]);