function Out = cellstrhcat(cellIn)
% Num of rows
numRows = size(cellIn,1);
% Preallocate
Out = cell(numRows ,1);
% Loop
for r = 1:numRows
    Out{r} = [cellIn{r,:}];
end
