function [limg_train, limg_test] = und_frgc_preproc(varargin)
p = inputParser;
addOptional(p, 'limg_train',[], @(x) isempty(x)||isa(x, 'Limage'))
addOptional(p, 'limg_test',[], @(x) isempty(x)||isa(x, 'Limage'))
parse(p, varargin{:})
limg_train = p.Results.limg_train;
limg_test = p.Results.limg_test;

% Add training from FRGC/UND
if isempty(limg_train)
    L = load('data\facial-landmarks\proc_DB_TRAIN\normals_interp.mat');
    limg_DB_TRAIN = L.limg_train;
    L = load('data\facial-landmarks\proc_DB45R\normals_interp.mat');
    limg_DB45R = L.limg_test;
    limg_DB45R = [limg_DB45R limg_DB45R.generate_additional_samples('mirror','und')];
    L = load('data\facial-landmarks\proc_DB45L\normals_interp.mat');
    limg_DB45L = L.limg_test;
    limg_DB45L = limg_DB45L.generate_additional_samples('mirror','und');
    L = load('data\facial-landmarks\proc_DB60R\normals_interp.mat');
    limg_DB60R = L.limg_test;
    limg_DB60R = [limg_DB60R limg_DB60R.generate_additional_samples('mirror','und')];
    L = load('data\facial-landmarks\proc_DB60L\normals_interp.mat');
    limg_DB60L = L.limg_test;
    limg_DB60L = [limg_DB60L limg_DB60L.generate_additional_samples('mirror','und')];
    
    limg_train = [limg_DB_TRAIN limg_DB45R limg_DB45L limg_DB60R limg_DB60L];
end
limg_train.adapt_labels_to_Bosphorus_notation()
[limg_train.orig_scale] = deal(1);

% Test set images
if isempty(limg_test)
    L = load('data\facial-landmarks\proc_DB45L\normals_interp.mat');%load('data\facial-landmarks\proc_DB45L\normals_interp.mat');
    limg_test = L.limg_test;
end
limg_test.adapt_labels_to_Bosphorus_notation()
[limg_test.orig_scale] = deal(1);

