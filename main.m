% Simultaneous multi-descent regression and feature learning for facial
% landmarking in depth images

%% Import data 

% Bosphorus 3D
[image_names_bosph_3D, lmrk_names_bosph_3D, image_names_bosph_2D, lmrk_names_bosph_2D] = io.file_paths_bosph();
limg = Limage(image_names_bosph_3D, lmrk_names_bosph_3D);
[limg_train, limg_test] = io.bosphorus_subsets(limg, 'subset','rotation45');

%% Pre-process images

% Normalize
limg_train.uint8gray()
limg_test.uint8gray()

% Augment training data with mirroring and rotating the existing images
limg_aug = limg_train.generate_additional_samples('mirror','Bosphorus', 'rotate',10);
limg_train = [limg_aug limg_train];

% Run face detector
limg_train.face_detect('clst');
limg_test.face_detect('clst');

% Remove misdetected objects
limg_train = limg_train.remove_misdetections();
limg_test = limg_test.remove_misdetections();

% Resize to standard bounding box size
limg_train.resize(100)
limg_test.resize(100)

%% Train landmarker
%lmodel = Smuf();
lmodel = Grid();

lmodel.train(copy(limg_train))

%% Run trained landmarker on test data

lmodel.verbose = 0;
lmodel.permuts = 1;
limg_test_copy = copy(limg_test);   
[lerr, serr] = lmodel.localize(limg_test_copy);
