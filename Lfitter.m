classdef Lfitter < handle
% LFITTER Parent class for a landmark fitting model

properties
    feature_type
    verbose
    iters
    lambda
    permuts
    block_size
    mean_var_norm
    pca_norm
end

properties(GetAccess = public, SetAccess = protected)
    reg
    mean_feat
    std_feat
    mean_loc
    lm_con
end

properties(Constant)
    labels = [{'Outer left eyebrow'};...     %1
              {'Middle left eyebrow'};...    %2
              {'Inner left eyebrow'};...     %3
              {'Inner right eyebrow'};...    %4
              {'Middle right eyebrow'};...   %5
              {'Outer right eyebrow'};...    %6
              {'Outer left eye corner'};...  %7
              {'Inner left eye corner'};...  %8
              {'Inner right eye corner'};... %9
              {'Outer right eye corner'};... %10
              {'Nose saddle left'};...       %11
              {'Nose saddle right'};...      %12
              {'Left nose peak'};...         %13
              {'Nose tip'};...               %14
              {'Right nose peak'};...        %15
              {'Left mouth corner'};...      %16
              {'Upper lip outer middle'};... %17
              {'Upper lip inner middle'};... %18
              {'Lower lip inner middle'};... %19
              {'Lower lip outer middle'};... %20
              {'Right mouth corner'};...     %21
              {'Chin middle'}];              %22
end

methods
    function self = Lfitter(varargin)
        p = inputParser;
        addParameter(p, 'iters', 7)
        addParameter(p, 'permuts', 1)
        addParameter(p, 'lambda', 1000)
        addParameter(p, 'feature_type', 'vhog');
        addParameter(p, 'block_size', 15);
        addParameter(p, 'mean_var_norm', 1);
        addParameter(p, 'pca_norm', {[]})
        addParameter(p, 'verbose', 1);
        parse(p, varargin{:})
        self.iters = p.Results.iters;
        self.permuts = p.Results.permuts;
        self.lambda = p.Results.lambda;
        self.feature_type = p.Results.feature_type;
        self.block_size = p.Results.block_size;
        self.verbose = p.Results.verbose;
        self.mean_var_norm = p.Results.mean_var_norm;
        self.pca_norm = p.Results.pca_norm;
        
        if length(self.lambda) == 1
            self.lambda = self.lambda*ones(self.iters, 1);
        end
        if length(self.block_size) == 1
            self.block_size = self.block_size*ones(self.iters, 1);
        end
    end
    
    function [feat, complete] = extract_feat(self, limg, varargin)
        p = inputParser;
        addRequired(p, 'self', @(x)isa(x, 'Lfitter'))
        addRequired(p, 'limg', @(x)isa(x, 'Limage'))
        addParameter(p,'lmark', 'm', @ischar)
        addParameter(p, 'trn', 0)
        addParameter(p, 'iter', 1, @(x) x > 0 && x <= self.iters)
        addParameter(p, 'reg', 1, @(x) x > 0)
        addParameter(p, 'merge', 'local')
        addParameter(p, 'permut', [])
        addParameter(p, 'norm', [])
        addParameter(p, 'pcan', 0)
        parse(p, self, limg, varargin{:})
        self = p.Results.self;
        limg = p.Results.limg;
        lmark = p.Results.lmark;
        trn = p.Results.trn;
        i = p.Results.iter;
        r = p.Results.reg;
        merge = p.Results.merge;
        if isempty(p.Results.permut)
            if trn
                npermuts = self.permuts;
            else
                npermuts = 1;
            end
        else
            npermuts = p.Results.permut;
        end
        if isempty(p.Results.norm)
            mvnorm = self.mean_var_norm;
        else
            mvnorm = p.Results.norm;
        end
        pcan = p.Results.pcan;
        
        if strcmp(self.feature_type, 'bdv') && isempty(self.lm_con)
            lmavg = limg.landmark_average();
            for l = 1 : length(lmavg)
                nnidx = knnsearch(lmavg, lmavg(l,:), 'K',15);
                self.lm_con(:,l) = nnidx(randperm(15-1,10)+1);
            end
        end
        
        if strcmp(self.feature_type, 'cv_surf')
            extractor = cv.DescriptorExtractor('SURF'); 
        end
        
        for c = 1 : length(limg)
            img = limg(c).data;
            if strcmp(lmark,'m')
                lmrk = limg(c).m_lmarks;
            else
                lmrk{1} = limg(c).gt_lmarks;
            end
            if self.block_size(i)>1
                blk_size = self.block_size(i);
            else
                blk_size = round(self.block_size(i)*mean([size(img,1),size(img,2)]));
            end
            [img, lmrk] = Lfitter.add_border(img, lmrk, 2*blk_size);
            for p = 1 : npermuts
                switch self.feature_type
                    case 'bdv'
                        tmp_feat = self.extractBDVFeatures(img, lmrk{p}, blk_size, self.lm_con);
                    case 'block'
                        bsize = blk_size + mod(blk_size-1,2);
                        tmp_feat = extractFeatures(img, lmrk{p}, ...
                            'Method','block', 'BlockSize',bsize)';
                    case 'freak'
                        bsize = blk_size + mod(blk_size-1,2);
                        tmp_feat = extractFeatures(img, lmrk{p}, ...
                            'Method','FREAK', 'BlockSize',bsize)';
                        tmp_feat = tmp_feat.Features';
                    case 'surf'
                        tmp_feat = extractFeatures(img,lmrk{p},...
                            'Method','SURF','Upright',true)';
                    case 'pdh'
                        tmp_feat = self.extractPDHFeatures(img, lmrk{p}, blk_size);
                    case 'ldv'
                        tmp_feat = self.extractLDVFeatures(img, lmrk{p});
                    case 'pdv'
                        tmp_feat = self.extractPDVFeatures(img, lmrk{p}, blk_size);
                    case 'pldv'
                        pdv = self.extractPDVFeatures(img, lmrk{p}, blk_size);
                        ldv = self.extractLDVFeatures(img, lmrk{p});
                        tmp_feat = [pdv; uint8(ldv); uint8(-ldv)];
                    case 'hog'
                        tmp_feat = extractHOGFeatures(img, lmrk{p},...
                            'CellSize', [round(blk_size/2) round(blk_size/2)],...
                            'BlockSize', [2 2],...
                            'UseSignedOrientation', true)';
                    case 'lbp'
                        tmp_feat = self.extractLBPfeatures(img, lmrk{p}, blk_size);
                    case 'ltp'
                        tmp_feat = self.extractLTPfeatures(img, lmrk{p}, blk_size);
                    case 'vhog'
                        img(isnan(img)) = 0;
                        tmp_feat = fd.MyextractHOGFeatures1(img, lmrk{p},...
                            [round(blk_size/2) round(blk_size/2)],...
                            [2 2], 0, 9, 1)';
                    case 'cv_surf'
                        cv_lmrk = cell2struct([num2cell(lmrk{p},2),...
                            repmat(num2cell(blk_size),size(lmrk{p},1),1)],...
                            [{'pt'};{'size'}], 2); 
                        tmp_feat = extractor.compute(img, cv_lmrk)';
                end
                feat{(c-1)*self.permuts+p} = tmp_feat;
            end
        end
        complete = cellfun('size', feat, 2) == size(limg(1).m_lmarks{1},1);
        if ~strcmp(merge, 'none')
            if strcmp(merge, 'global')
                feat = cellfun(@(x)x(:), feat(complete), 'UniformOutput', false);
            elseif strcmp(merge, 'local')
                feat = feat(complete);
            end
            feat = cell2mat(feat);
            if isa(feat,'uint8')
                feat = single(feat);
            end
        end

        if pcan
            if true
                if trn
                    [coeff, ~, ~, ~, explained] = pca(feat');
                    len = cumsum(explained) < 95;
                    self.pca_norm{r,i} = coeff(:, len);
                end
                feat = self.pca_norm{r,i}*(self.pca_norm{r,i}'*feat);
            else
                if strcmp(merge, 'global')
                    feat = reshape(feat, size(tmp_feat,1), []);
                end
                if trn
                    grnd = repmat((1:size(tmp_feat,2))', size(feat,2)/size(tmp_feat, 2), 1);
                    opts.Fisherface = 0;
                    opts.PCARatio = .95;
                    [eigvec, ~] = fd.lda(grnd, opts, feat');
                    self.pca_norm{r,i} = eigvec;
                end
                feat = self.pca_norm{r,i}'*feat;
                if strcmp(merge, 'global')
                    feat = reshape(feat, [], length(limg));
                end
            end
        end
        
        if trn
            self.mean_feat{r,i} = mean(feat, 2);
            self.std_feat{r,i} = std(feat, [], 2);
        end
        if mvnorm
            feat = (feat - repmat(self.mean_feat{r,i}, 1, size(feat, 2)));%./...
%                     repmat(self.std_feat{r,i}, 1, size(feat, 2));
        end 

    end
    
    function self_copy = copy(self)
        wid = 'MATLAB:structOnObject';
        warning('off',wid)
        for c = 1 : length(self)
            self_copy(c) = feval(class(self(c)));
            p = fieldnames(struct(self(c)));
            for i = 1:length(p)
                self_copy(c).(p{i}) = self(c).(p{i});
            end
        end
        warning('on',wid)
    end
    
    function webcam_localize(self, bb2lm, varargin)
        p = inputParser;
        addRequired(p, 'self', @(x)isa(x, 'Lfitter'))
        addRequired(p, 'bb2lm', @isstruct)
        addOptional(p, 'tracking', 0, @isnumeric)
        addOptional(p, 'bb_size', 128, @isnumeric)
        parse(p, self, bb2lm, varargin{:})
        self = p.Results.self;
        bb2lm = p.Results.bb2lm;
        track = p.Results.tracking;
        bb_size = p.Results.bb_size;
        
        fh = figure('keypressfcn','close','windowstyle','modal');
        
        self.verbose = 0;
        
        cam = webcam('Logitech');
        if track, lmarks_prev = []; end
        
        while true
            t1 = clock;
            img = snapshot(cam);
            img = fliplr(img);
            limg = Limage(img);
            limg.face_detect();
            if ishandle(fh)
                if ~isempty(limg.bbox)
                    limg.resize(bb_size)
                    limg.uint8gray()
                    if track &&...
                       ~isempty(lmarks_prev) &&...
                       (max(lmarks_prev{1}(:))-min(lmarks_prev{1}(:)))<limg.bbox(3)*1.5 &&...
                       sqrt(sum((mean(lmarks_prev{1}) - limg.bbox(1:2)-limg.bbox(3)/2).^2)) < limg.bbox(3)/2
                            limg.m_lmarks = lmarks_prev;
                    else
                        limg.lmarks_from_bb(bb2lm)
                    end
                    imshow(img,[])
                    hold on
                    self.localize(limg);
                    if track
                        lmarks_prev = limg.m_lmarks;
                    end
                    rectangle('Position', [limg.bbox(1),limg.bbox(2),...
                              limg.bbox(3),limg.bbox(4)]./limg.orig_scale,'EdgeColor','r')
                    m_lmark = limg.m_lmarks{1}./limg.orig_scale;
                    if length(m_lmark) == 68
                        plot(m_lmark(1 :17,1), m_lmark(1 :17,2),...
                             m_lmark(18:22,1), m_lmark(18:22,2),...
                             m_lmark(23:27,1), m_lmark(23:27,2),...
                             m_lmark(28:31,1), m_lmark(28:31,2),...
                             m_lmark(32:36,1), m_lmark(32:36,2),...
                             m_lmark([37:42,37],1), m_lmark([37:42,37],2),...
                             m_lmark([43:48,43],1), m_lmark([43:48,43],2),...
                             m_lmark(37:42,1), m_lmark(37:42,2),...
                             m_lmark([49:60,49],1), m_lmark([49:60,49],2),...
                             m_lmark([61:68,61],1), m_lmark([61:68,61],2),...
                             'Color', 'g')
                    elseif ~isempty(m_lmark)
                        plot(m_lmark(:,1), m_lmark(:,2), '.g')
                    end
                    hold off
                else
                    imshow(img)
                    title('No face found by face detector')
                    if track, lmarks_prev = []; end
                end
            else
                clear cam
                return
            end
            t2 = clock;
            set(fh,'Name', sprintf('FPS: %.f', 60/etime(t2,t1)))
        end
    end

end % methods

methods(Static)
    
    function bdv = extractBDVFeatures(img, lmrk, bsize, lmcon)
    % Block difference features
        radius = round((bsize - 1)/2); 
        step = floor(bsize/4);
        lmrk_tmp = round(lmrk);
        lmrk_tmp((lmrk_tmp - radius) < 1) = radius + 1;
        lmrk_tmp((lmrk_tmp(:,1) + radius) > size(img, 2), 1) = ...
            size(img, 2) - radius;
        lmrk_tmp((lmrk_tmp(:,2) + radius) > size(img, 1), 2) = ...
            size(img, 1) - radius;
        for l = 1 : size(lmrk_tmp, 1)
            patch = img(lmrk_tmp(l,2)-radius : step : lmrk_tmp(l,2)+radius, ...
                        lmrk_tmp(l,1)-radius : step : lmrk_tmp(l,1)+radius);
            patches(:, l) = patch(:);
        end
        for l = 1 : length(lmrk)
            for n = 1 : size(lmcon,1)
                bdv(1+(n-1)*size(patches,1):n*size(patches,1), l) = ...
                    patches(:,l) - patches(:,lmcon(n,l));
            end
        end
    end
    
    function lbp = extractLBPfeatures(img, lmrk, bsize)
    % Local binary patterns
        radius = round((bsize - 1)/2);
        lmrk_tmp = round(lmrk);
        lmrk_tmp((lmrk_tmp - radius) < 1) = radius + 1;
        lmrk_tmp((lmrk_tmp(:,1) + radius) > size(img, 2), 1) = ...
            size(img, 2) - radius;
        lmrk_tmp((lmrk_tmp(:,2) + radius) > size(img, 1), 2) = ...
            size(img, 1) - radius;
        for l = 1 : size(lmrk_tmp,1)
            patch = img(lmrk_tmp(l,2)-radius : lmrk_tmp(l,2)+radius, ...
                               lmrk_tmp(l,1)-radius : lmrk_tmp(l,1)+radius, :);
            lbp(:, l) = extractLBPFeatures(patch,'CellSize',floor(size(patch)/3),'NumNeighbors',8,'Interpolation','Nearest','Upright',true);
        end
    end
    
    function ltp = extractLTPfeatures(img, lmrk, bsize)
    % Local ternary patterns
        radius = round((bsize - 1)/2);
        lmrk_tmp = round(lmrk);
        lmrk_tmp((lmrk_tmp - radius) < 1) = radius + 1;
        lmrk_tmp((lmrk_tmp(:,1) + radius) > size(img, 2), 1) = ...
            size(img, 2) - radius;
        lmrk_tmp((lmrk_tmp(:,2) + radius) > size(img, 1), 2) = ...
            size(img, 1) - radius;
        for l = 1 : size(lmrk_tmp,1)
            patch = single(img(lmrk_tmp(l,2)-radius : lmrk_tmp(l,2)+radius, ...
                               lmrk_tmp(l,1)-radius : lmrk_tmp(l,1)+radius, :));
            %ltp_tmp = fd.LBP(patch, 1);
            %[ltp_up, ltp_low] = fd.LTP(patch, 10);
            %ltp_tmp = [ltp_up(:); ltp_low(:)];
            %tlp_tmp = histcounts(ltp_tmp,255);
            ltp_tmp = desc_LTeP(patch,struct('gridHist',2));
            ltp(:, l) = ltp_tmp;
        end
    end
    
    function pdh = extractPDHFeatures(img, lmrk, bsize)
    % Pixel difference histograms
        blkfun = @(x) x([1 2 3 4 6 7 8 9])-x(5);
        radius = round((bsize - 1)/2);
        step = ceil(bsize/10);
        lmrk_tmp = round(lmrk);
        lmrk_tmp((lmrk_tmp - radius) < 1) = radius + 1;
        lmrk_tmp((lmrk_tmp(:,1) + radius) > size(img, 2), 1) = ...
            size(img, 2) - radius;
        lmrk_tmp((lmrk_tmp(:,2) + radius) > size(img, 1), 2) = ...
            size(img, 1) - radius;
        for l = 1 : size(lmrk_tmp,1)
            patch = single(img(lmrk_tmp(l,2)-radius : lmrk_tmp(l,2)+radius, ...
                               lmrk_tmp(l,1)-radius : lmrk_tmp(l,1)+radius, :));
            xt = 0;
            for xb = 2 : step : size(patch, 1)-1
                xt = xt + 1;
                yt = 0;
                for yb = 2 : step : size(patch, 2)-1
                    yt = yt + 1;
                    blk(:,xt,yt) = blkfun(patch(xb-1:xb+1,yb-1:yb+1));
                end
            end
            pdh(:, l) = blk(:);
        end
    end
    
    function pdv = extractPDVFeatures(img, lmrk, bsize)
    % Pixel difference values
        radius = round((bsize - 1)/2); 
        lmrk_tmp = round(lmrk);
        lmrk_tmp((lmrk_tmp - radius) < 1) = radius + 1;
        lmrk_tmp((lmrk_tmp(:,1) + radius) > size(img, 2), 1) = ...
            size(img, 2) - radius;
        lmrk_tmp((lmrk_tmp(:,2) + radius) > size(img, 1), 2) = ...
            size(img, 1) - radius;
        for l = 1 : size(lmrk_tmp, 1)
            %cent = img(lmrk_tmp(l,2),lmrk_tmp(l,1));
            patch = img(unique(round(linspace(lmrk_tmp(l,2)-radius, lmrk_tmp(l,2)+radius, round(bsize*.6)))), ...
                        unique(round(linspace(lmrk_tmp(l,1)-radius, lmrk_tmp(l,1)+radius, round(bsize*.6)))));
%             patch = img(unique(round(linspace(lmrk_tmp(l,2)-radius, lmrk_tmp(l,2)+radius, 10))), ...
%                         unique(round(linspace(lmrk_tmp(l,1)-radius, lmrk_tmp(l,1)+radius, 10))));
            patch = patch(:);
            %patch(ceil(length(patch)/2)) = [];
            pdv(:,l) = patch;%[patch - cent; cent - patch];
        end
    end
    
    function ldv = extractLDVFeatures(img, lmrk)
    % Landmark difference values
        lmrk_tmp = round(lmrk);
        lmrk_tmp(lmrk_tmp < 1) = 1;
        lmrk_tmp(lmrk_tmp(:,1)>size(img,2), 1) = size(img, 2);
        lmrk_tmp(lmrk_tmp(:,2)>size(img,1), 2) = size(img, 1);
        lmrk_val = single(img(sub2ind(size(img), lmrk_tmp(:,2), lmrk_tmp(:,1))));
        for l = 1 : size(lmrk_tmp, 1)
            val = lmrk_val;
            val(l) = [];
            ldv(:,l) = lmrk_val(l)-val;
        end
    end
    
    function [img, lmrk] = add_border(img, lmrk, border_size)
        img = padarray(img,[border_size border_size], mode(img(:)), 'both');
        for p = 1 : length(lmrk)
            lmrk{p} = lmrk{p} + border_size.*ones(size(lmrk{p}));
        end
    end
    
    function merr = ced_curve(limg, varargin)
    % Error metric is computed as an average Euclidean distance 
    % between the 29 annotated and predicted landmarks, normalized by 
    % the interocluar distance.
    % 
    % [1] P. N. Belhumeur, D. W. Jacobs, D. J. Kriegman, and N. Kumar.
    %     Localizing parts of faces using a consensus of exemplars. 
    %     In CVPR, 2011.

        p = inputParser;
        addRequired(p, 'limg', @(x)isa(x, 'Limage'))
        addOptional(p, 'norm', 1)
        addOptional(p, 'idx', [])
        parse(p, limg, varargin{:})
        limg = p.Results.limg;
        norm = p.Results.norm;
        idx = p.Results.idx;
    
        if isempty(idx)
            % 29 selected landmarks
            idx = [18,27,22,23,20,21,24,25,37,46,40,43,38,41,45,48,42,47,32,...
                   36,31,34,49,55,52,63,67,58,9];
        end
        
        % Compute mean error over 29 landmarks for each test image
        err = zeros(length(limg),1);
        eye_dist = zeros(length(limg),1);
        for c = 1 : length(limg)
            err(c) = mean(...
                          sqrt(sum((limg(c).gt_lmarks(idx,:)-...
                                    limg(c).m_lmarks{1}(idx,:)).^2,2))...
                          )/limg(c).orig_scale;
            if length(idx) == 68
                eye_dist(c) = sqrt(sum((mean(limg(c).gt_lmarks(37:42,:))-...
                                        mean(limg(c).m_lmarks{1}(43:48,:))).^2))...
                                        /limg(c).orig_scale;
            elseif length(idx) == 22
                eye_dist(c) = sqrt(sum((mean(limg(c).gt_lmarks([1:3,7,8],:))-...
                                        mean(limg(c).m_lmarks{1}([4:6,9,10],:))).^2))...
                                        /limg(c).orig_scale;
            else
                error('Eye distance is not defined yet.')
            end
        end
        
        % Mean error over all test images
        if norm
            merr = mean(err./eye_dist)*100;
        else
            merr = mean(err);
        end
        
        % Cumulative error distribution curve
        err = err./eye_dist;
        xe = linspace(min(err),max(err),100);
        ye(size(xe)) = 0;
        for c = 1 : length(xe)
            ye(c) = sum(err <= xe(c))/length(err);
        end

        figure
        plot(xe*100,ye,'LineWidth',1)
        xlabel('Normalised mean error [%]')
        ylabel('Fraction of Test Faces')
        title('Cumulative Error Distribution')
        axis([0 100 0 1])
        grid on

    end
    
    function B = dropout(B,prob)
        di = false(size(B,1),1);
        d = randperm(size(B,1),round(size(B,1)*prob));
        di(d) = true;
        B(repmat(di,1,size(B,2))) = 0.5;
    end
    
    function [f, varargout] = normalize_mean_var(f, varargin)
        if nargin == 1
            varargout{1} = nanmean(f,2);
            xstd = nanstd(f,0,2); xstd(xstd==0) = eps;
            varargout{2} = xstd; 
            f = (f-repmat(varargout{1},1,size(f,2)))./...
                repmat(varargout{2},1,size(f,2));
        else
            f = (f-repmat(varargin{1}, 1,size(f,2)))./...
                repmat(varargin{2}, 1,size(f,2));
        end
    end
    
end % static methods

end % classdef