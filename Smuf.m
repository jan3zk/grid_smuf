classdef Smuf < Lfitter
% Simultaneous learning of MUltiple-descent directions and binary Features

properties(GetAccess = public, SetAccess = protected)
    w
    pca_norm_bin
    mean_lmarks
    bb2lm
    reg_labels
    mean_bgate
    std_bgate
    mean_gt
end

methods
    function self = Smuf()
        self.feature_type = 'pdv';
        self.permuts = 15;
        self.lambda = linspace(1e4, 1e3, self.iters);
        self.block_size = round(linspace(40, 20, self.iters));
    end
    
    function train(self, limg)
        
        p = inputParser;
        addRequired(p, 'self', @(x)isa(x, 'Lfitter'))
        addRequired(p, 'limg', @(x)isa(x, 'Limage'))
        parse(p, self, limg)
        self = p.Results.self;
        limg = p.Results.limg;
        
        if length(self.lambda) == 1
            self.lambda = self.lambda*ones(self.iters, 1);
        end
        
        for r = 1 : 5
            
            switch r
                case 1
                    fprintf('\nTrain frontal model...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'LFAU_'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'UFAU_'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'CAU_'))==1 | ...
                                 cellfun(@length,strfind({limg.image_fpath},'R10'))==1 | ...
                                 cellfun(@length,strfind({limg.image_fpath},'L10'))==1 | ...
                                 cellfun(@length,strfind({limg.image_fpath},'R20'))==1 | ...
                                 cellfun(@length,strfind({limg.image_fpath},'L20'))==1);
                    self.reg_labels{r} = self.labels; %Bosphorus
                    %self.reg_labels{r} = self.labels([7:10,14,16,21:22]); %FRGC+UND
                case 2
                    fprintf('\nTrain model facing left...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'L30'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'L45'))==1);
                    self.reg_labels{r} = self.labels([1:3 7:8 11 13:14 16:20 22]); %Bosphorus
                    %self.reg_labels{r} = self.labels([7:8,14,16,22]); %FRGC+UND
                case 3
                    fprintf('\nTrain model facing right...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'R30'))==1 |...
                                 cellfun(@length,strfind({limg.image_fpath},'R45'))==1);
                    self.reg_labels{r} = self.labels([4:6 9:10 12 14:15 17:22]); %Bosphorus
                    %self.reg_labels{r} = self.labels([9:10,14,21,22]); %FRGC+UND
                case 4
                    fprintf('\nTrain left profile model...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'L90'))==1);
                    self.reg_labels{r} = self.labels([1:3 7 11 13:14 16:20 22]); %Bosphorus
                    %self.reg_labels{r} = self.labels([7:8,14,16,22]); %FRGC+UND
                case 5
                    fprintf('\nTrain right profile model...\n')
                    limgr = limg(cellfun(@length,strfind({limg.image_fpath},'R90'))==1);
                    self.reg_labels{r} = self.labels([4:6 10 12 14:15 17:22]); %Bosphorus
                    %self.reg_labels{r} = self.labels([9:10,14,21,22]); %FRGC+UND
            end
            
            limgr = limgr.adjust_points_and_labels(self.reg_labels{r});
            self.bb2lm{r} = limgr.lmarks_from_bb();
            if self.permuts > 1
                limgr.random_sampling(self.bb2lm{r}, self.permuts)
            end
            
            if self.verbose
                rng('shuffle')
                ridx = randi(length(limgr));
                fh = figure;
                set(fh, 'Name', 'Initial state of a random training image')
                limgr(ridx).view('permuts', self.permuts)
                fh = figure('units','normalized','outerposition',[0 0 1 1]);
                set(fh,'Name','Point convergence on a random training image') 
            end

            % Matrix of ground truth locations
            gt_lmrk = zeros(length(limgr(1).gt_lmarks(:)),...
                            self.permuts*length(limgr));
            for c = 1 : length(limgr)
                for p = 1 : self.permuts
                    gt_lmrk(:, (c-1)*self.permuts+p) = limgr(c).gt_lmarks(:);
                end
            end

            g1=.1; g2=-.1;
            for i = 1 : self.iters

                fprintf('\nTrain landmarker (iteration %d/%d)\n', i, self.iters)
                
                self.mean_gt{r,i} = mean(extract_feat(self, limgr, 'iter',i, 'lmark','gt', 'norm',0, 'pcan',0),2);

                % Get matrix of update locations from training data
                m_lmrk = zeros(size(gt_lmrk));
                for c = 1 : length(limgr)
                    for p = 1 : self.permuts
                        m_lmrk(:, (c-1)*self.permuts+p) = limgr(c).m_lmarks{p}(:);
                    end
                end

                % Extract features
                fprintf('Computing features ...\n')
                [X, complete] = extract_feat(self, limgr, 'trn', 1, 'iter', i, 'reg',r, 'norm',0);
                %X = X - repmat(reshape(self.mean_gt{r},size(X,1),[]),1,size(X,2)/size(m_lmrk,1)*2);
                X = X - repmat(self.mean_gt{r,i},1,size(X,2));
                
                % Dimensionality reduction (PCA)
                %[coeff, ~, ~, ~, explained] = pca(X');
                %len = cumsum(explained) < 95;
                %self.pca_norm{r,i} = coeff(:, len);
                %X = self.pca_norm{r,i}'*X;
                
                % Location difference
                dY = gt_lmrk(:, complete) - m_lmrk(:, complete);
                self.mean_loc{r,i} = mean(dY, 2);
                dY = dY - repmat(self.mean_loc{r,i}, 1, size(dY, 2));

                % Simultaneously learn regressor and binary features
                rng('default'); rng(1)
                W = orth(randn(size(X, 1), 15)); %W = orth(randn(size(X, 1), round(size(X,1)/15))); 
                for t = 1 : 5
                    B = .5*( sign(W'*X) + 1 );
                    B = reshape(B, length(limgr(1).gt_lmarks)*size(W,2), length(limgr)*self.permuts);
                    WtX = reshape(W'*X,[],length(limgr)*self.permuts);
                    p1 = (B-.5-WtX)*(B-.5-WtX)';
                    p2 = (B-.5)*(B-.5)';
                    R = dY*B'/(B*B' + self.lambda(i)*eye(size(B,1)) +...
                        g1.*p1 + .1*min(diag(p1)).*eye(size(B,1)) + ...
                        g2.*p2 + .1*min(diag(p2)).*eye(size(B,1)));
                    epsR = .1*min(diag(R*R'));
                    A = R'/(R*R'+epsR*eye(size(R,1)))*dY +...
                        g1*(B-.5) + ...
                        g2*.5;
                    A = reshape(A,size(A,1)/length(limgr(1).gt_lmarks),[]); 
                    epsX = .1*min(diag(X*X'));
                    Wold = W;
                    W = (A*X'/(X*X'+epsX*eye(size(X,1))))'./(1+g1+g2);
                    fprintf('Change of W: %f\n',sqrt(trace((Wold-W)*(Wold-W)')))
                end
                self.reg{r,i} = R;
                self.w{r,i} = W;
                
                if i == 1
                    B_gate = .5*( W'*X + 1 );
                    B_gate = reshape(B_gate, length(limgr(1).gt_lmarks)*size(W,2), length(limgr)*self.permuts);
                    self.mean_bgate{r} = mean(B_gate, 2);
                    self.std_bgate{r} = std(B_gate,[],2);
                end

                % Update landmarks with the trained regressor
                m_lmrk(:, complete) = m_lmrk(:, complete) + self.reg{r,i}*B(:,complete) + ...
                         repmat(self.mean_loc{r,i}, 1, size(dY, 2));

                % Localization error
                err = mean(sqrt(mean((gt_lmrk(:, complete)-...
                                      m_lmrk(:, complete)).^2, 2)));
                fprintf('Localization error: %f\n', err)

                % Write updated locations to training data
                for c = 1 : length(limgr)
                    for p = 1 : self.permuts
                        limgr(c).m_lmarks{p} = ...
                            reshape(m_lmrk(:, (c-1)*self.permuts+p), [], 2);
                    end
                end

                if self.verbose
                    sh = subplot(2, ceil(self.iters/2), i);
                    limgr(ridx).view('permuts', self.permuts)
                    title(sh, sprintf('Iteration %d', i))
                    drawnow
                end
            end
        end
        
    end

    function [loc_err, sel_err] = localize(self, limg)
        
        if self.verbose
            fh = figure('units','normalized','outerposition',[0 0 1 1]);
            set(fh, 'Name', 'Localize landmarks using trained landmarker')
        end
        
        tm = [];
        cr = zeros(1,length(limg));
        for c = 1 : length(limg)
            
            if length(limg) > 1
                fprintf('Landmark localization %d/%d\n', c, length(limg))
            end

            for i = 1 : self.iters
                
                % Find regressor with gating function
                if i == 1
                   
                   mahal_dist = NaN.*ones(5,self.permuts);
                   for r = 1 : 5
                       limg(c).lmarks_from_bb(self.bb2lm{r})
                       if self.permuts > 1
                           limg(c).random_sampling(self.bb2lm{r}, self.permuts)
                       end
                       [X, ~] = extract_feat(self, limg(c), 'iter', i, 'reg',r, 'permut',self.permuts);
                       %X = self.pca_norm{r,i}'*X;
                       B_gate = .5*( self.w{r,i}'*X + 1 );
                       S = 1./(self.std_bgate{r}.^2);
                       mu = self.mean_bgate{r};
                       for p = 1 : self.permuts
                            B = B_gate(:,1+(p-1)*length(limg(c).m_lmarks{p}):p*length(limg(c).m_lmarks{p}));
                            md = (B(:)-mu)'*diag(S)*(B(:)-mu);
                            mahal_dist(r,p) = 1/length(self.reg_labels{r})*sqrt(md);
                       end
                   end
                   [~,cr(c)] = min(min(mahal_dist,[],2));
                   limg(c).lmarks_from_bb(self.bb2lm{cr(c)})
                   if self.verbose
                        sh = subplot(2, ceil((self.iters+1)/2), 1);
                        limg(c).view()
                        title(sh, 'Initialization')
                        drawnow
                    end
                end
                
                [X, complete] = extract_feat(self, limg(c), 'iter', i, 'reg',cr(c),'norm',0);
                X = X - repmat(self.mean_gt{cr(c),i},1,size(X,2));
               
               %X = self.pca_norm{cr(c),i}'*X;
               
               if all(complete)
                    
                   % Binary features
                   B = .5*( sign(self.w{cr(c),i}'*X) + 1 );
                   tic
                   % Udate landmarks
                   limg(c).m_lmarks{1} = reshape(...
                       limg(c).m_lmarks{1}(:) + ...
                       self.reg{cr(c),i}*B(:) + ...
                       self.mean_loc{cr(c),i},...
                                      size(limg(c).m_lmarks{1}, 1), 2);
                   tm = [tm toc];
               else
                   fprintf(['Locations (obj %d, iter %d) aren''t updated, ', ...
                           'since some of the features couldn''t be extracted.\n'], c, i)
               end
               if self.verbose
                   sh = subplot(2, ceil((self.iters+1)/2), i+1);
                   limg(c).view()
                   title(sh, sprintf('State after iteration %d', i))
                   drawnow
               end

            end
            
            if self.verbose
                pause
            end
            
        end
        fprintf('Average time for feature extraction: %d\n', mean(tm))
        
        % Check for model selection errors
        sel_err = ...
        ( (cellfun(@length,strfind({limg.image_fpath},'L10')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'L20')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'L30')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 ) &...
          (cr == 3 | cr == 5) ) |...
        ( (cellfun(@length,strfind({limg.image_fpath},'R10')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R20')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R30')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R90')) == 1 ) &...
          (cr == 2 | cr == 4) ) |...
        ( (cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 |...
           cellfun(@length,strfind({limg.image_fpath},'R90')) == 1 ) &...
           cr == 1 ) |...
        ( ~(cellfun(@length,strfind({limg.image_fpath},'L20')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'L30')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'L45')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'L90')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'R20')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'R30')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'R45')) == 1 |...
            cellfun(@length,strfind({limg.image_fpath},'R90')) == 1) &...
           (cr == 4 | cr == 5) );
        sel_err = sum(sel_err)/length(limg)*100;
        
        % Localization error
        loc_err = NaN*ones(length(self.labels),length(limg));
        for c = 1 : length(limg)
            if ~isempty(limg(c).m_lmarks{1}) && ~isempty(limg(c).gt_lmarks)
                limg(c).adjust_points_and_labels(self.reg_labels{cr(c)},2);
                err_tmp = sqrt(sum((limg(c).gt_lmarks - limg(c).m_lmarks{1}).^2, 2));
                [~,idx] = ismember(limg(c).lmark_labels,self.labels);
                loc_err(idx,c) = err_tmp./limg(c).orig_scale;
            end
        end
        loc_err = [nanmean(loc_err,2) nanstd(loc_err,0,2)];
        loc_err = nanmean(loc_err);
    end
    
end

end
